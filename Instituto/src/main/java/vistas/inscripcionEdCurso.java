package vistas;
import javax.persistence.EntityManager;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import controladores.cursoController;
import controladores.edicionCursoController;
import controladores.estudianteController;
import controladores.inscripcionEdCursoController;
import controladores.institutoBrindaCursoController;
import controladores.institutoController;
import progApp.Todo.Curso;
import progApp.Todo.Instituto;
import progApp.Todo.InstitutoBrindaCurso;
import progApp.Todo.MenuPrincipal;
import progApp.Todo.edicionCurso;
import progApp.Todo.Estudiante;

import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JButton;


public class inscripcionEdCurso extends JFrame {
	private String cursoSeleccionado, instSeleccionado, edicionSeleccionada, estudianteSeleccionado, instituto;
	private JPanel contentPane;
	private static EntityManager manager;
	private static EntityManager emf;
	private JList listaCursos, listaEstudiantes ;//listaEdiciones
	private JList listaEdicion;
	private JList listaEstudiante;
	private JFrame frame;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					inscripcionEdCurso frame = new inscripcionEdCurso();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public inscripcionEdCurso() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 767, 592);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		listaEdicion = new JList();
		listaEstudiantes = new JList();
		institutoController controladorInstitutos = new institutoController();
		cursoController controladorCurso = new cursoController();
		edicionCursoController controladorEdCurso = new edicionCursoController();
		estudianteController controladorEstudiantes = new estudianteController();
		inscripcionEdCursoController controladorInscripcionEdCurso = new inscripcionEdCursoController();
		institutoBrindaCursoController controladorInstBrindaCurso = new institutoBrindaCursoController();
		JTextArea textEdCurso = new JTextArea();
		JTextArea textEstudiante = new JTextArea();
		JLabel lblFechaDisplay = new JLabel(java.time.LocalDate.now().toString());
		
		/* el combo box va a tener la lista de los institutos que existen */

		ArrayList<Instituto> institutos = controladorInstitutos.listarInstitutos();
		int tamanio = institutos.size();
		
		String[] array = new String[tamanio+1];	
		array[0]="Seleccione instituto";
		
		for (int i = 0; i < institutos.size(); i++) {
			array[i+1] = institutos.get(i).getNombreInstituto();
			
		}
		
		JComboBox comboInstitutos = new JComboBox(array);
		
		comboInstitutos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JComboBox<String> fnt = (JComboBox<String>)e.getSource();
				JComboBox<String> comboBox1 = fnt;
				
				instituto = (String)comboBox1.getSelectedItem();
		
				ArrayList<InstitutoBrindaCurso> cursos = controladorInstBrindaCurso.obtenerCursos(instituto);
				//aca control
				
				DefaultListModel hola = new DefaultListModel();
						
				for (int i = 0; i< cursos.size(); i++) {
					hola.addElement(cursos.get(i).getNombreCurso());
				}
		
				listaCursos.setModel(hola);
				
			}
		});
		comboInstitutos.setBounds(10, 21, 173, 22);
		contentPane.add(comboInstitutos);
//***************************************************************************************************************/
		ArrayList<Curso> cursos = controladorCurso.listarCursos(instSeleccionado);
		int tamCur = cursos.size();
		String[] arrCur = new String[tamCur+1];
		arrCur[0]="Seleccione el curso";
		for(int i=0; i < cursos.size();i++) {
			arrCur[i+1] = cursos.get(i).getNombre();
		}
		
		listaCursos = new JList();
		listaCursos.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent me) {
	             if (me.getClickCount() == 1) {
	             
	                  cursoSeleccionado = listaCursos.getSelectedValue().toString();
	                  
			 		
					  ArrayList<edicionCurso> cursos = controladorEdCurso.listarEdCursos(cursoSeleccionado);
								
					  DefaultListModel hola = new DefaultListModel();
								
				      for (int i = 0; i< cursos.size(); i++) {
				    	if(controladorEdCurso.edicionesVigentes(java.time.LocalDate.now(),cursos.get(i).getFechaIni(), cursos.get(i).getFechaFin())){
				    		hola.addElement(cursos.get(i).getNomCurso());
				    	}
					
				      }
					/* El administrador elige un CURSO y el sistema muestra la edici�n vigente de curso asociado, si existe*/
				
				      listaEdicion.setModel(hola);
	             }
	          }
	       });

		
		listaCursos.setBounds(10, 77, 173, 452);
		contentPane.add(listaCursos);
		
		/* selecciona un curso de la lista y se muestran las ediciones que tiene ese curso */
		
		listaEdicion.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent me) {
	             if (me.getClickCount() == 1) {
	              
	                  edicionSeleccionada = listaEdicion.getSelectedValue().toString();
	                  textEdCurso.setText(edicionSeleccionada);
	             }
	          }
	       }); 
		
		listaEdicion.setBounds(211, 77, 179, 452);
		contentPane.add(listaEdicion);
	
		ArrayList<Estudiante> estudiantes = controladorEstudiantes.listarEstudiantes();
		System.out.println("Tiene..?"+estudiantes.size());
		DefaultListModel hola2 = new DefaultListModel();
		for (int i = 0; i < estudiantes.size(); i++) {
			hola2.addElement(estudiantes.get(i).getNombre());;
			
		}
		
		listaEstudiantes.setModel(hola2);
		listaEstudiantes.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent me) {
	             if (me.getClickCount() == 1) {
	              
	                  estudianteSeleccionado = listaEstudiantes.getSelectedValue().toString();
	                  textEstudiante.setText(estudianteSeleccionado);
			 		   }
	         }
	       });       
		listaEstudiantes.setBounds(415, 77, 167, 452);
		contentPane.add(listaEstudiantes);
		
		JLabel lblTitulo = new JLabel("Datos de Inscripci\u00F3n:");
		lblTitulo.setBounds(602, 78, 124, 14);
		contentPane.add(lblTitulo);
		
		JLabel lblEdicionCurso = new JLabel("Edicion de curso:");
		lblEdicionCurso.setBounds(592, 132, 118, 14);
		contentPane.add(lblEdicionCurso);
		
		
		textEdCurso.setBounds(592, 157, 151, 22);
		contentPane.add(textEdCurso);
		
		JLabel lblEstudiante = new JLabel("Estudiante:");
		lblEstudiante.setBounds(592, 199, 66, 14);
		contentPane.add(lblEstudiante);
		
		
		textEstudiante.setBounds(592, 224, 151, 22);
		contentPane.add(textEstudiante);
		
		JLabel lblFecha = new JLabel("Fecha de inscripci\u00F3n:");
		lblFecha.setBounds(592, 268, 151, 14);
		contentPane.add(lblFecha);
		
		
		lblFechaDisplay.setBounds(592, 291, 151, 22);
		contentPane.add(lblFechaDisplay);
		
		JButton btnAceptar = new JButton("Inscribir");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean resultado;
				resultado = controladorInscripcionEdCurso.altaInscripEdCurso(textEdCurso.getText(),textEstudiante.getText(),java.time.LocalDate.now());
				if(resultado== false) {
					JOptionPane.showMessageDialog(null, "La Edicion de curso fu� agregada con �xito");
				}
				else{
					String[] opciones = {"Modificar", "Cancelar"};
					int opc = JOptionPane.showOptionDialog(null, "La edici�n ya existe", "Elija la opci�n", JOptionPane.DEFAULT_OPTION,JOptionPane.INFORMATION_MESSAGE, null, opciones, opciones[1]);
					// si le da a cancelar que levante la ventana devuelta
					System.out.println(opc);
					if(opc == 1) {
						//levanto devuelta la ventana del menu principal
						dispose();
						MenuPrincipal menu = new MenuPrincipal();
						frame.setContentPane(menu);
						frame.validate();
						
						
						
					}
						
					
				}
			}
		});
		
		btnAceptar.setBounds(621, 344, 89, 23);
		contentPane.add(btnAceptar);
		
		JLabel lblListaCursos = new JLabel("< Cursos >");
		lblListaCursos.setBounds(50, 54, 89, 14);
		contentPane.add(lblListaCursos);
		
		JLabel lblListaEdiciones = new JLabel("< Edici\u00F3n de Curso >");
		lblListaEdiciones.setBounds(246, 54, 118, 14);
		contentPane.add(lblListaEdiciones);
		
		JLabel lblListaEstudiantes = new JLabel("< Estudiantes >");
		lblListaEstudiantes.setBounds(452, 54, 98, 14);
		contentPane.add(lblListaEstudiantes);
		
		
		
		
	
}
	
}
