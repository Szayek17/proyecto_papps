package vistas;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import controladores.cursoController;
import controladores.edicionCursoController;
import controladores.institutoBrindaCursoController;
import controladores.institutoController;
import progApp.Todo.Curso;

import progApp.Todo.edicionCurso;
import progApp.Todo.Instituto;
import progApp.Todo.InstitutoBrindaCurso;

import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.persistence.EntityManager;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JButton;



public class consultaEdicionCurso extends JFrame {
	private String instSeleccionado, cursoSeleccionado, edicionSeleccionada, instituto;
	private ArrayList<Curso> cursos = new ArrayList<Curso>();
	private String[] arrayCur;	
	private JList listaCursos, listaEdiciones;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static EntityManager manager;
	private static EntityManager emf;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					consultaEdicionCurso frame = new consultaEdicionCurso();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public consultaEdicionCurso() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 769, 590);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		institutoController controladorInstitutos = new institutoController();
		cursoController controladorCurso = new cursoController();
		edicionCursoController controladorEdCurso = new edicionCursoController();
		institutoBrindaCursoController controladorInstBrindaCurso = new institutoBrindaCursoController();
		
		listaEdiciones = new JList();
		JTextArea textNombre = new JTextArea();
		JTextArea textCupos = new JTextArea();
		JTextArea textFini = new JTextArea();
		JTextArea textFin = new JTextArea();
		JTextArea textFechaPubli = new JTextArea();
		
		
		ArrayList<Instituto> institutos = controladorInstitutos.listarInstitutos();
		int tamanio = institutos.size();
		
		String[] array = new String[tamanio+1];	
		array[0]="Seleccione instituto";
		
		for (int i = 0; i < institutos.size(); i++) {
			array[i+1] = institutos.get(i).getNombreInstituto();
			
		}
		
		JComboBox comboInstitutos = new JComboBox(array);
		
		comboInstitutos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JComboBox<String> fnt = (JComboBox<String>)e.getSource();
				JComboBox<String> comboBox1 = fnt;
				
				instituto = (String)comboBox1.getSelectedItem();
		
				ArrayList<InstitutoBrindaCurso> cursos = controladorInstBrindaCurso.obtenerCursos(instituto);
				//aca control
				
				DefaultListModel hola = new DefaultListModel();
						
				for (int i = 0; i< cursos.size(); i++) {
					hola.addElement(cursos.get(i).getNombreCurso());
				}
		
				listaCursos.setModel(hola);
				
			}
		});
		comboInstitutos.setBounds(10, 21, 173, 22);
		contentPane.add(comboInstitutos);

		ArrayList<Curso> cursos = controladorCurso.listarCursos(instSeleccionado);
		int tamCur = cursos.size();
		String[] arrCur = new String[tamCur+1];
		arrCur[0]="Seleccione el curso";
		for(int i=0; i < cursos.size();i++) {
			arrCur[i+1] = cursos.get(i).getNombre();
		}
		
		listaCursos = new JList();
		listaCursos.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent me) {
	             if (me.getClickCount() == 1) {
	             
	                  cursoSeleccionado = listaCursos.getSelectedValue().toString();
			 		
					  ArrayList<edicionCurso> cursos = controladorEdCurso.listarEdCursos(cursoSeleccionado);
								
					  DefaultListModel hola = new DefaultListModel();
								
				      for (int i = 0; i< cursos.size(); i++) {
						hola.addElement(cursos.get(i).getNomEdicion());
				      }
						
				      listaEdiciones.setModel(hola);
	             }
	          }
	       });

		
		listaCursos.setBounds(10, 77, 173, 452);
		contentPane.add(listaCursos);
		
		/* selecciona un curso de la lista y se muestran las ediciones que tiene ese curso */
		
		listaEdiciones.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent me) {
	             if (me.getClickCount() == 1) {
	              
	                  edicionSeleccionada = listaEdiciones.getSelectedValue().toString();
	                  System.out.println("La edicion seleccionada es " + edicionSeleccionada);
	                 			 		
					  ArrayList<edicionCurso> datosCursos = controladorEdCurso.listarDatosEdCursos(edicionSeleccionada);
					  System.out.println("Datos Trae" + datosCursos.toString());
					  textNombre.setText(datosCursos.get(0).getNomCurso());
					  textCupos.setText(String.valueOf(datosCursos.get(0).getCupos()));
					  textFini.setText(datosCursos.get(0).getFechaIni().toString());
					  textFin.setText(datosCursos.get(0).getFechaFin().toString());
					  textFechaPubli.setText(datosCursos.get(0).getFechaPublicado().toString());
	
	             }
	          }
	       });
		listaEdiciones.setVisibleRowCount(20);
		listaEdiciones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listaEdiciones.setBounds(206, 77, 194, 452);
		contentPane.add(listaEdiciones);
		
		JLabel lblCursos = new JLabel("Cursos del Instituto: ");
		lblCursos.setBounds(10, 52, 173, 14);
		contentPane.add(lblCursos);
		
		JLabel lblNewLabel = new JLabel("Ediciones del Curso: (Doble Click)");
		lblNewLabel.setBounds(206, 52, 183, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(441, 78, 46, 14);
		contentPane.add(lblNombre);
		
		JLabel lblCupos = new JLabel("Cupos");
		lblCupos.setBounds(441, 103, 46, 14);
		contentPane.add(lblCupos);
		
		JLabel lblFechaIni = new JLabel("Fecha Inicio");
		lblFechaIni.setBounds(441, 128, 79, 14);
		contentPane.add(lblFechaIni);
		
		JLabel lblFechaFin = new JLabel("Fecha Fin");
		lblFechaFin.setBounds(441, 153, 46, 14);
		contentPane.add(lblFechaFin);
		
		JLabel lblFechaPubli = new JLabel("Fecha Publicado");
		lblFechaPubli.setBounds(441, 178, 79, 14);
		contentPane.add(lblFechaPubli);
		
		
		textNombre.setBounds(536, 73, 183, 19);
		contentPane.add(textNombre);
		
		
		textCupos.setBounds(536, 98, 183, 19);
		contentPane.add(textCupos);

		textFini.setBounds(536, 123, 183, 19);
		contentPane.add(textFini);
		
		
		textFin.setBounds(536, 148, 183, 19);
		contentPane.add(textFin);
		
		
		textFechaPubli.setBounds(536, 173, 183, 19);
		contentPane.add(textFechaPubli);
		
		JLabel lblDEC = new JLabel("Datos de la edicion de curso");
		lblDEC.setBounds(492, 52, 227, 14);
		contentPane.add(lblDEC);
		
		JButton btnAtras = new JButton("Atras");
		btnAtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		btnAtras.setBounds(630, 223, 89, 23);
		contentPane.add(btnAtras);
		
	}
}
