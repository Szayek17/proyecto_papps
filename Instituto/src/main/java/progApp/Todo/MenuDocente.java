package progApp.Todo;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;



import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;

public class MenuDocente extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
    Connection con=null;
    PreparedStatement ps=null;
    ResultSet rs=null;
    private static EntityManager manager;
    private static EntityManagerFactory emf;
    private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuDocente frame = new MenuDocente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuDocente() {
        emf = Persistence.createEntityManagerFactory("Institutos");
        manager = emf.createEntityManager();
		setTitle("Registrar Docente");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 303);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(167, 38, 247, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(167, 69, 247, 20);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(167, 100, 247, 20);
		panel.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(167, 131, 247, 20);
		panel.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(167, 162, 247, 20);
		panel.add(textField_4);
		
		JLabel lblNewLabel = new JLabel("Usuario");
		lblNewLabel.setBounds(10, 41, 94, 14);
		panel.add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 72, 115, 14);
		panel.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(10, 103, 127, 14);
		panel.add(lblApellido);
		
		JLabel lblCorreoElectronico = new JLabel("Correo Electronico");
		lblCorreoElectronico.setBounds(10, 134, 147, 14);
		panel.add(lblCorreoElectronico);
		
		JLabel lblFecha = new JLabel("Instituto");
		lblFecha.setBounds(10, 165, 127, 14);
		panel.add(lblFecha);
		
		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SimpleDateFormat Datefor = new SimpleDateFormat("yyyy/MM/dd"); 
				Date utilDate;
				try {
					utilDate = Datefor.parse(textField_5.getText());
					Usuario registrado = new Usuario(textField.getText(), textField_1.getText(), textField_2.getText(), textField_3.getText(), utilDate);
					if (manager.find(Usuario.class, registrado.getCorreo()) == null) {
						Query query = manager.createQuery("INSERT INTO docente (nickname, nombre, apellido, correo, instituto, fecha) VALUES (?1, ?2, ?3, ?4, ?5, ?6)");
						manager.getTransaction().begin();
						query.setParameter(1, textField.getText()); //nickname
						query.setParameter(2, textField_1.getText());  // nombre
						query.setParameter(3, textField_2.getText()); //apellido
						query.setParameter(4, textField_3.getText()); //correo
						query.setParameter(5, textField_4.getText()); //instituto
						query.setParameter(6, utilDate); //fecha
						query.executeUpdate();

						manager.getTransaction().commit();
	                    manager.getTransaction().begin();
	                    manager.merge(registrado);
	                    manager.getTransaction().commit();
	                    JOptionPane.showMessageDialog((Component) e.getSource(),
	                        "Docente registrado correctamente",
	                        "Operacion Exitosa",
	                        JOptionPane.INFORMATION_MESSAGE);
	                    textField_2.setText("");        
	                    textField_3.setText("");
	                    textField_4.setText("");
	                    textField_1.setText("");
	                    textField.setText("");

	                } else {
	                	JOptionPane.showMessageDialog((Component) e.getSource(),
	                            "Docente ya registrado",
	                            "ERROR",
	                            JOptionPane.INFORMATION_MESSAGE);
	                }
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				

			}
		});
		btnNewButton.setBounds(325, 231, 89, 23);
		panel.add(btnNewButton);
		
		JLabel lblNewLabel_1 = new JLabel("Registro de Docentes");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblNewLabel_1.setBounds(167, 0, 247, 27);
		panel.add(lblNewLabel_1);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(225, 231, 89, 23);
		panel.add(btnCancelar);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(167, 193, 247, 20);
		panel.add(textField_5);
		
		JLabel lblFecha_1 = new JLabel("Fecha (Ej. 2020/10/30)");
		lblFecha_1.setBounds(10, 196, 127, 14);
		panel.add(lblFecha_1);
	}
}

