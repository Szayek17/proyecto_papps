package progApp.Todo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import controladores.cursoController;

import javax.swing.JButton;
import javax.swing.JComboBox;

public class IF_ElegirCurso extends JInternalFrame {
	
	private static final long serialVersionUID = 1L;
	private int cancelo=-1;
	private static cursoController controlador = new cursoController();
	
	public IF_ElegirCurso(List<String> insCursos) {
		setTitle("Consulta de Curso");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 440, 268);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JComboBox<String> cmbCursos = new JComboBox<String>();
		cmbCursos.setBounds(130, 125, 179, 24);
		panel.add(cmbCursos);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(281, 215, 104, 25);
		panel.add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(48, 215, 117, 25);
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cancelo=1;
				cerrar();
			}
		});
		panel.add(btnCancelar);
		
		JLabel lblSeleccioneElCurso = new JLabel("Seleccione el curso del que desea ver la información:");
		lblSeleccioneElCurso.setBounds(29, 54, 376, 15);
		panel.add(lblSeleccioneElCurso);
		
		if(insCursos.size()>0) {
			for(String c : insCursos)
				cmbCursos.addItem(c);
			
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					
					IF_VerCurso ventana3 = new IF_VerCurso(controlador.elegirCurso((String)cmbCursos.getSelectedItem()));
					ventana3.setSize(760, 700);
					ventana3.setLocation(0,0);
					ConsultaCurso.escritorio.add(ventana3);
					cerrar();
					ventana3.setVisible(true);
					cancelo=0;
				}
			});
		} else {
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					cancelo=1;
					cerrar();
				}
			});
			cmbCursos.addItem("-Nada para mostrar-");
			JOptionPane.showMessageDialog(null, "Este instituto no tiene cursos asociados.");
			cancelo=1;
		}
	}
	
	public int getCancelo() {
		return cancelo;
	}
	
	private void cerrar() {
		this.dispose();
	}
}