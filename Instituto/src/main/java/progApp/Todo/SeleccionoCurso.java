package progApp.Todo;

//import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
//import javax.swing.JOptionPane;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;


public class SeleccionoCurso extends JFrame {

	private JPanel contentPane;
	private static EntityManager manager;
	private static EntityManagerFactory emf;
	private static final long serialVersionUID = 1L;
    Connection con=null;
    PreparedStatement ps=null;
    PreparedStatement ps2=null;
    ResultSet rs=null;
    private static String nombreForm;
    //ResultSet rs2=null;
    ArrayList<String> myList = new ArrayList<>();
	JList<?> listaCursos;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SeleccionoCurso frame = new SeleccionoCurso(" ");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void DbConnection(){
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto","root","agusabE123@#");
			
		}catch(Exception e){ System.out.println(e);}  
	}

	/**
	 * Create the frame.
	 */
	public SeleccionoCurso(String nomForm) {
		emf = Persistence.createEntityManagerFactory("Institutos");
		manager = emf.createEntityManager();
		nombreForm = nomForm;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Salir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
			}
			
		});
		btnNewButton.setBounds(0, 238, 217, 23);
		contentPane.add(btnNewButton);
		DbConnection();
//		list = new JList<Object>(myList.toArray());
//	     list.addMouseListener(new MouseAdapter() {
//	    	 public void mouseClicked(MouseEvent evt) {
//	    		 JList<?> list = (JList<?>)evt.getSource();
//	    	        if (evt.getClickCount() == 2) {
//
//	    	            // Double-click detected
//	    	            int index = list.locationToIndex(evt.getPoint());
//	    	               if (index >= 0) {
//	    	                  Object item = list.getModel().getElementAt(index);
//	    	                  String usuario = item.toString();
//	    	                  String[] parts = usuario.split(" ");
//	    	                  //String nom = parts[0];
//	    	                  //String ape = parts[1];
//	    	           	    try{
//	    	           	    	
//	    	       	    	 ps=con.prepareStatement("select * from curso");
//	    	                 rs=ps.executeQuery();
//	    	                    if(rs.next()) {
//	    	                    	 String nombre = (rs.getString(1)); 
//	    	                         Integer duracion = (rs.getInt(2));
//	    	                         Integer horas = (rs.getInt(3));
//	    	                         Integer creditos = (rs.getInt(4));
//	    	                         String duracion1 = String.valueOf(duracion);
//	    	                         String horas1 = String.valueOf(horas);
//	    	                         String creditos1 = String.valueOf(creditos);
//	    	                         String completo = "Cursos: " + "\nNombre: " + nombre + "\n" + "Duracion: " +duracion1 + "\n" + "Horas: " + horas1 + "\n" + "Creditos: " +creditos1;
//		    	                     JOptionPane.showMessageDialog(null, completo, "Datos de Docente", JOptionPane.INFORMATION_MESSAGE);
//	    	                    }
//	    	               }
//	    	               catch(Exception e)
//	    	               {
//	    	                   System.out.println("Error in getData"+e);
//	    	               }
//	    	                   
//	    	                }
//	    	        } 
//	    	 }
//	     });
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 434, 239);
		contentPane.add(scrollPane);
		try {
			ps = con.prepareStatement("select nomCurso from curso;");
			rs = ps.executeQuery();
			while (rs.next()) {
				String listado = (rs.getString(1));
				myList.add(listado);
			}
			con.close();

		} catch (Exception e) {
			System.out.println("Error en obtener los datos" + e);
		}
		
		
		listaCursos = new JList<Object>(myList.toArray());
		listaCursos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listaCursos.setBorder(new LineBorder(new Color(0, 0, 0)));
		listaCursos.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		scrollPane.setViewportView(listaCursos);
		
		JButton btnNewButton_1 = new JButton("Aceptar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String curso = listaCursos.getSelectedValue().toString();
				System.out.println(curso);
				System.out.println(nombreForm);
				CursosIntProgramas cursosProgramas = new CursosIntProgramas(curso, nombreForm);
				manager.getTransaction().begin();
				manager.persist(cursosProgramas);
				manager.getTransaction().commit();
				
				JOptionPane.showMessageDialog((Component) e.getSource(),
						"Curso registrado en el programa correctamente", "Operacion Exitosa",
						JOptionPane.INFORMATION_MESSAGE);
				
				
			}
		});
		btnNewButton_1.setBounds(217, 238, 217, 23);
		contentPane.add(btnNewButton_1);
	}

}
