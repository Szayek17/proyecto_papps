package progApp.Todo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name = "docente")
@Table(name = "docente")
public class Docente extends Usuario {
	@Column(name = "instituto")
	private String instituto;
	
	public Docente() {
		
	}
	
	public Docente(String nickname,String nombre,String apellido, String Correo,String instituto, Date fecha)
	{
		super(nickname,nombre,apellido,Correo, fecha);
		this.instituto=instituto;
	}



	
	//VERIFICAR COMO ES DOCENTE O NO
	
	
}

