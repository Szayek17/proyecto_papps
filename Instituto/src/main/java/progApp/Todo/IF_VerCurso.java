package progApp.Todo;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import controladores.CursosIntProgramasController;
import controladores.Prog_FormacionController;
import controladores.cursoController;
import controladores.edicionCursoController;
import controladores.institutoBrindaCursoController;

import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IF_VerCurso extends JInternalFrame {
	
	private boolean cerrar;
	private static final long serialVersionUID = 1L;
	private static cursoController cursoControlador = new cursoController();
	private static institutoBrindaCursoController institutoBrindaCursoControlador = new institutoBrindaCursoController();
	private static edicionCursoController edicionCursoControlador = new edicionCursoController();

	public IF_VerCurso(Curso curso) {
		if(curso!=null) {
			setTitle("Datos del curso elegido");
			setBounds(100, 100, 760, 700);
			getContentPane().setLayout(null);
			
			JLabel lbl_nombre = new JLabel("Nombre");
			lbl_nombre.setBounds(22, 34, 70, 15);
			getContentPane().add(lbl_nombre);
			
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(32, 54, 215, 37);
			getContentPane().add(scrollPane);
			
			JTextArea txt_nombre = new JTextArea();
			txt_nombre.setEditable(false);
			scrollPane.setViewportView(txt_nombre);
			if(curso.getNombre()!=null) {
				txt_nombre.setText(curso.getNombre());
				
				JLabel lbl_duracion = new JLabel("Duración");
				lbl_duracion.setBounds(22, 95, 70, 15);
				getContentPane().add(lbl_duracion);
				
				JScrollPane scrollPane_1 = new JScrollPane();
				scrollPane_1.setBounds(32, 112, 215, 37);
				getContentPane().add(scrollPane_1);
				
				JTextArea txt_duracion = new JTextArea();
				txt_duracion.setEditable(false);
				scrollPane_1.setViewportView(txt_duracion);
				if(curso.getDuracion()!=null)
					txt_duracion.setText(curso.getDuracion() + " semana/s.");
				else
					txt_duracion.setText("¡Este curso no tiene una duración asignada!");
				
				JLabel lbl_horas = new JLabel("Horas");
				lbl_horas.setBounds(22, 153, 70, 15);
				getContentPane().add(lbl_horas);
				
				JScrollPane scrollPane_1_1 = new JScrollPane();
				scrollPane_1_1.setBounds(32, 171, 215, 37);
				getContentPane().add(scrollPane_1_1);
				
				JTextArea txt_horas = new JTextArea();
				txt_horas.setEditable(false);
				scrollPane_1_1.setViewportView(txt_horas);
				txt_horas.setText(Integer.toString(curso.getCantHoras()));
				
				JLabel lbl_creditos = new JLabel("Créditos");
				lbl_creditos.setBounds(22, 210, 70, 15);
				getContentPane().add(lbl_creditos);
				
				JScrollPane scrollPane_1_1_1 = new JScrollPane();
				scrollPane_1_1_1.setBounds(32, 230, 215, 37);
				getContentPane().add(scrollPane_1_1_1);
				
				JTextArea txt_creditos = new JTextArea();
				txt_creditos.setEditable(false);
				scrollPane_1_1_1.setViewportView(txt_creditos);
				if(curso.getCantCreditos()!=-1)
					txt_creditos.setText(Integer.toString(curso.getCantCreditos()));
				else
					txt_creditos.setText("¡Este curso no tiene créditos asignados!");
				
				JLabel lbl_previas = new JLabel("Previas");
				lbl_previas.setBounds(372, 251, 70, 15);
				getContentPane().add(lbl_previas);
				
				JScrollPane scrollPane_1_1_1_1_1_2 = new JScrollPane();
				scrollPane_1_1_1_1_1_2.setBounds(384, 285, 215, 74);
				getContentPane().add(scrollPane_1_1_1_1_1_2);
				
				JTextArea txt_previas = new JTextArea();
				txt_previas.setLineWrap(true);
				txt_previas.setWrapStyleWord(true);
				txt_previas.setEditable(false);
				scrollPane_1_1_1_1_1_2.setViewportView(txt_previas);
				List<String> prevs = cursoControlador.listarPrevias(curso.getNombre());
				if(prevs.size()>0) {
					for(String c : prevs) {
						txt_previas.append(c + "\n");
					}
				}
				else {
					txt_previas.setText("Este curso no tiene previas.");
				}
				
				JLabel lbl_fRegistro = new JLabel("Fecha Registro");
				lbl_fRegistro.setBounds(22, 271, 122, 15);
				getContentPane().add(lbl_fRegistro);
				
				JScrollPane scrollPane_1_1_1_1 = new JScrollPane();
				scrollPane_1_1_1_1.setBounds(32, 291, 215, 37);
				getContentPane().add(scrollPane_1_1_1_1);
				
				JTextArea txt_fecha = new JTextArea();
				txt_fecha.setEditable(false);
				scrollPane_1_1_1_1.setViewportView(txt_fecha);
				if(curso.getFechaPublicado()!=null) {
//					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//					sdf.format((curso.getFechaPublicado()))
					txt_fecha.setText(curso.getFechaPublicado().toString());
				}
				else
					txt_fecha.setText("¡Este curso no tiene fecha asignada!");
				
				JLabel lbl_instituto = new JLabel("Instituto");
				lbl_instituto.setBounds(22, 332, 70, 15);
				getContentPane().add(lbl_instituto);
				
				JScrollPane scrollPane_1_1_1_1_1 = new JScrollPane();
				scrollPane_1_1_1_1_1.setBounds(32, 351, 215, 37);
				getContentPane().add(scrollPane_1_1_1_1_1);
				
				JTextArea txt_instituto = new JTextArea();
				txt_instituto.setEditable(false);
				scrollPane_1_1_1_1_1.setViewportView(txt_instituto);

				txt_instituto.setText(institutoBrindaCursoControlador.obtenerInstituto(curso.getNombre()));
					
				JLabel lbl_desc = new JLabel("Descripción");
				lbl_desc.setBounds(372, 34, 88, 15);
				getContentPane().add(lbl_desc);
					
				JScrollPane scrollPane_1_1_1_1_1_1 = new JScrollPane();
				scrollPane_1_1_1_1_1_1.setBounds(384, 61, 284, 167);
				getContentPane().add(scrollPane_1_1_1_1_1_1);
					
				JTextArea txt_desc = new JTextArea();
				txt_desc.setWrapStyleWord(true);
				txt_desc.setLineWrap(true);
				txt_desc.setEditable(false);
				scrollPane_1_1_1_1_1_1.setViewportView(txt_desc);
				if(curso.getDescripcion()!=null)
					txt_desc.setText(curso.getDescripcion());
				else
					txt_desc.setText("¡Este curso no tiene descripción!");
					
				JLabel lbl_url = new JLabel("URL");
				lbl_url.setBounds(372, 382, 70, 15);
				getContentPane().add(lbl_url);
				
				JScrollPane scrollPane_1_1_1_1_1_3 = new JScrollPane();
				scrollPane_1_1_1_1_1_3.setBounds(384, 409, 215, 37);
				getContentPane().add(scrollPane_1_1_1_1_1_3);
					
				JTextArea txt_url = new JTextArea();
				txt_url.setEditable(false);
				scrollPane_1_1_1_1_1_3.setViewportView(txt_url);
				if(curso.getUrl()!=null)
					txt_url.setText(curso.getUrl());
				else
					txt_url.setText("¡Este curso no tiene URL asignada!");
					
				JLabel lbl_progF = new JLabel("Programas de Formación");
				lbl_progF.setBounds(22, 409, 193, 15);
				getContentPane().add(lbl_progF);
					
				JComboBox<String> cmb_progF = new JComboBox<String>();
				cmb_progF.setBounds(32, 436, 205, 24);
				List<String> listaP = CursosIntProgramasController.listarProgramasDeCurso(curso.getNombre());
					
				if(listaP.size()>0) {
					cmb_progF.addItem("-Ninguno seleccionado-");
					for(String p : listaP) {
						cmb_progF.addItem(p);
					}
				}
				else 
					cmb_progF.addItem("Sin programas de formación.");
					
				getContentPane().add(cmb_progF);
					
				JLabel lbl_EdC = new JLabel("Ediciones de Curso");
				lbl_EdC.setBounds(22, 509, 193, 15);
				getContentPane().add(lbl_EdC);
					
				JComboBox<String> cmb_ediC = new JComboBox<String>();
				cmb_ediC.setBounds(32, 536, 205, 24);
					
				List<edicionCurso> listaE = edicionCursoControlador.listarEdCursos(curso.getNombre());
					
				if(listaE.size()>0) {
					cmb_ediC.addItem("-Ninguno seleccionado-");
					for(edicionCurso e : listaE) {
						cmb_ediC.addItem(e.getNomEdicion());
					}
				}
				else 
					cmb_ediC.addItem("Sin ediciones.");
					
				getContentPane().add(cmb_ediC);
					
				JButton btn_visu = new JButton("Visualizar");
				btn_visu.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if(cmb_progF.getSelectedItem().equals("Sin programas de formación.") || 
								cmb_ediC.getSelectedItem().equals("Sin ediciones.")) {
							JOptionPane.showMessageDialog(null, "No hay registrados programas de formación o ediciones de este curso para visualizar.");
							cerrar=false;
						}
									
						if(cmb_progF.getSelectedItem().equals("-Ninguno seleccionado-") 
								&& !cmb_ediC.getSelectedItem().equals("-Ninguno seleccionado-")) {

//Ver datos de la edición del curso-----------------------------------------------------------------------------------------------------------------------------							
							//Se llama a listar ediciones de curso.
							edicionCurso edicion = edicionCursoControlador.elegirEdicion((String)cmb_ediC.getSelectedItem());
							if(edicion!=null) {
								String cuposString;
								if(edicion.getCupos()==-1)
									cuposString = "Sin cupos disponibles.";
								else
									cuposString = Integer.toString(edicion.getCupos());
							
								String completo = "Nombre: " + edicion.getNomEdicion() + "\n" + "Fecha de inicio: " + edicion.getFechaIni().toString() + "\n"
										+ "Fecha de fin: " + edicion.getFechaFin().toString() + "\n" + "Fecha de Publicación: " + edicion.getFechaPublicado().toString() + "\n"
										+ "Cupos: " + cuposString;
								JOptionPane.showMessageDialog(null, completo, "Datos de la Edición del curso", JOptionPane.INFORMATION_MESSAGE);
							} else {
								JOptionPane.showMessageDialog(null, "No se recibió la edición.", "ERROR INTERNO", JOptionPane.ERROR_MESSAGE);
							}
//Fin ver datos de la edición del curso-----------------------------------------------------------------------------------------------------------------------------							
						}
						else if(!cmb_progF.getSelectedItem().equals("-Ninguno seleccionado-") 
								&& cmb_ediC.getSelectedItem().equals("-Ninguno seleccionado-")) {
//Ver datos del programa de formación-----------------------------------------------------------------------------------------------------------------------------
							//Se llama a listar Programas de Formación.
							Prog_Formacion programa = Prog_FormacionController.elegirPrograma(cmb_progF.getSelectedItem().toString());
							if(programa!=null) {
								String completo = "Nombre: " + programa.getNombreForm() + "\n" + "Descripción: " + programa.getDesc()
								 + "\n" + "Fecha de inicio: " + programa.getfIni() + "\n"+ "Fecha de fin: " + programa.getfFin();
								JOptionPane.showMessageDialog(null, completo, "Datos del Programa de formación", JOptionPane.INFORMATION_MESSAGE);
							} else {
								JOptionPane.showMessageDialog(null, "No se recibió el programa.", "ERROR INTERNO", JOptionPane.ERROR_MESSAGE);
							}
//Fin ver datos del programa de formación-----------------------------------------------------------------------------------------------------------------------------
						}
						else if(!cmb_progF.getSelectedItem().equals("-Ninguno seleccionado-") 
								&& !cmb_ediC.getSelectedItem().equals("-Ninguno seleccionado-")
								&& !cmb_progF.getSelectedItem().equals("Sin programas de formación.") 
								&& !cmb_ediC.getSelectedItem().equals("Sin ediciones.")) {
							cerrar=false;
							JOptionPane.showMessageDialog(null, "No puede visualizar los datos de ambos a la vez.");
						}
						else if(cmb_progF.getSelectedItem().equals("-Ninguno seleccionado-") 
								&& cmb_ediC.getSelectedItem().equals("-Ninguno seleccionado-")) {
							cerrar=false;
							JOptionPane.showMessageDialog(null, "ERROR: Para visualizar debe elegir uno de las listas desplegables.");;
						}
					}
				});
				btn_visu.setBounds(439, 548, 117, 25);
				getContentPane().add(btn_visu);
				
				JLabel lblNewLabel = new JLabel("Para visualizar los datos de un Programa de ");
				lblNewLabel.setBounds(311, 459, 370, 15);
				getContentPane().add(lblNewLabel);
				
				JLabel lblNewLabel_1 = new JLabel("Formación o una Edición de Curso, eliga uno de");
				lblNewLabel_1.setBounds(311, 478, 370, 15);
				getContentPane().add(lblNewLabel_1);
					
				JLabel lblLasListasDesplegables = new JLabel("las listas desplegables y presione en \"Visualizar\".");
				lblLasListasDesplegables.setBounds(311, 495, 370, 15);
				getContentPane().add(lblLasListasDesplegables);
					
				JButton btnSalir = new JButton("Salir");
				btnSalir.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						cerrar();
						ConsultaCurso.escritorio=null;
					}
				});
				btnSalir.setBounds(322, 630, 117, 25);
				getContentPane().add(btnSalir);

			}
			else {
				JOptionPane.showMessageDialog(null, "¡Este curso no tiene un nombre asignado!");
				cerrar=true;
			}
		} else {
			JOptionPane.showMessageDialog(null, "¡Este curso no pertenece al Instituto seleccionado!");
			cerrar=true;
		}
	}
	
	public void cerrar() {
		dispose();
	}
	
	public boolean getCerrar() {
		return cerrar;
	}
}
