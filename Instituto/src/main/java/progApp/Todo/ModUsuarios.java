package progApp.Todo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class ModUsuarios extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModUsuarios frame = new ModUsuarios();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
    Connection con=null;
    PreparedStatement ps=null;
    PreparedStatement ps2=null;
    ResultSet rs=null;
    ResultSet rs2=null;
    int rs3;
    ArrayList<String> myList = new ArrayList<>();
    private static EntityManagerFactory emf;
	JList<?> list = new JList<Object>();
	@SuppressWarnings("rawtypes")
	DefaultListModel model = new DefaultListModel();

	/**
	 * Create the frame.
	 */
	public void Dbconnection()
    {
       try{

               Class.forName("com.mysql.jdbc.Driver");
               con=DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto","root","agusabE123@#");            }
           catch(Exception e)
           {
               System.out.println("Error in connection"+e);
           }
   }

	@SuppressWarnings("unchecked")
	public ModUsuarios() {
		setTitle("Modificacion de Usuario");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
        emf = Persistence.createEntityManagerFactory("Institutos");
        emf.createEntityManager();
		contentPane.setLayout(null);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(5, 5, 426, 230);
		contentPane.add(scrollPane);
		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(5, 240, 426, 23);
		btnSalir.setFont(new Font("Trebuchet MS", Font.BOLD, 11));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
			}
		});
		contentPane.add(btnSalir);
		Dbconnection();
	    try{
	    	 ps=con.prepareStatement("select * from usuario");
              
              rs=ps.executeQuery();
              while(rs.next())
              {
                  String nombre = (rs.getString(2)); 
                  String apellido = (rs.getString(3));
                  String completo = nombre + " " + apellido;
                  model.addElement(completo);
              }

        }
        catch(Exception e)
        {
            System.out.println("Error in getData"+e);
        }
	     list = new JList<Object>(model);
	     list.setBorder(new LineBorder(new Color(0, 0, 0)));
	     list.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	     scrollPane.setViewportView(list);
	     list.addMouseListener(new MouseAdapter() {
	    	 public void mouseClicked(MouseEvent evt) {
	    		 JList<?> list = (JList<?>)evt.getSource();
	    	        if (evt.getClickCount() == 2) {

	    	            // Double-click detected
	    	            int index = list.locationToIndex(evt.getPoint());
	    	               if (index >= 0) {
	    	                  Object item = list.getModel().getElementAt(index);
	    	                  String usuario = item.toString();
	    	                  String[] parts = usuario.split(" ");
	    	                  String nom = parts[0];
	    	                  String ape = parts[1];
	    	                  JFrame users = new JFrame();
	    	          		JPanel panel2 = new JPanel();
	    	          		users.getContentPane().add(panel2, BorderLayout.CENTER);
	    	        		users.setBounds(100, 100, 450, 180);

	    	        		panel2.setLayout(null);
	    	        		
	    	        		JTextField textField = new JTextField();
	    	        		textField.setBounds(116, 11, 300, 20);
	    	        		panel2.add(textField);
	    	        		textField.setColumns(10);
	    	        		
	    	        		JTextField textField_1 = new JTextField();
	    	        		textField_1.setColumns(10);
	    	        		textField_1.setBounds(116, 42, 300, 20);
	    	        		panel2.add(textField_1);
	    	        		
	    	        		JTextField textField_2 = new JTextField();
	    	        		textField_2.setColumns(10);
	    	        		textField_2.setBounds(116, 73, 300, 20);
	    	        		panel2.add(textField_2);
	    	        		
	    	        		JLabel lblNewLabel = new JLabel("Nuevo Nombre");
	    	        		lblNewLabel.setBounds(10, 14, 96, 14);
	    	        		panel2.add(lblNewLabel);
	    	        		
	    	        		JLabel lblNuevaFecha = new JLabel("Nuevo Apellido");
	    	        		lblNuevaFecha.setBounds(10, 45, 96, 14);
	    	        		panel2.add(lblNuevaFecha);
	    	        		
	    	        		JLabel label = new JLabel("Nueva Fecha");
	    	        		label.setBounds(10, 76, 96, 14);
	    	        		panel2.add(label);
	    	        		
	    	        		JButton btnNewButton = new JButton("Aceptar");
	    	        		btnNewButton.addActionListener(new ActionListener() {
	    	        			public void actionPerformed(ActionEvent e) {
	    	    	        		String nuevoNombre = textField.getText();
	    	        				String nuevoApellido = textField_1.getText();
	    	        				String utilDate = textField_2.getText();
	    	    	           	    try{
		    	    					utilDate = utilDate.replace('/', '-');
	    		    	       	    	 ps=con.prepareStatement("select * from docente where nombre = '" + nom + "' and apellido = '" + ape + "'");
	    		    	       	    	 ps2=con.prepareStatement("select * from estudiante where nombre = '" + nom + "' and apellido = '" + ape + "'");
	    		    	                 rs=ps.executeQuery();
	    		    	                 rs2 = ps2.executeQuery();
	    		    	                    if(rs2.next()) {
	    		    	                    	ps2=con.prepareStatement("update estudiante set nombre = '" + nuevoNombre + "', apellido = '" + nuevoApellido + "', fecha = '" + utilDate + "' where nombre = '" + nom + "' and apellido = '" + ape + "'");
	    		    	                    	ps=con.prepareStatement("update usuario set nombre = '" + nuevoNombre + "', apellido = '" + nuevoApellido + "', fecha = '" + utilDate +"' where nombre = '" + nom + "' and apellido = '" + ape + "'");
	    		    	                    	rs3 = ps.executeUpdate();
	    		    	                    	rs3 = ps2.executeUpdate();
	    		    	                    	
	    		    	                    }
	    		    	                    if(rs.next()) {
	    		    	                    	ps2=con.prepareStatement("update docente set nombre = '" + nuevoNombre + "', apellido = '" + nuevoApellido + "', fecha = '" + utilDate +"' where nombre = '" + nom + "' and apellido = '" + ape + "'");
	    		    	                    	ps=con.prepareStatement("update usuario set nombre = '" + nuevoNombre + "', apellido = '" + nuevoApellido + "', fecha = '" + utilDate +"' where nombre = '" + nom + "' and apellido = '" + ape + "'");
	    		    	                    	rs3 = ps.executeUpdate();
	    		    	                    	rs3 = ps2.executeUpdate();
	    		    	                    }
	    		    	                    model.removeAllElements();
	    		    	            	    try{
	    		    	           	    	 ps=con.prepareStatement("select * from usuario");
	    		    	                         
	    		    	                         rs=ps.executeQuery();
	    		    	                         while(rs.next())
	    		    	                         {
	    		    	                             String nombre = (rs.getString(2)); 
	    		    	                             String apellido = (rs.getString(3));
	    		    	                             String completo = nombre + " " + apellido;
	    		    	                             model.addElement(completo);
	    		    	                         }

	    		    	                   }
	    		    	                   catch(Exception e1)
	    		    	                   {
	    		    	                       System.out.println("Error in getData"+e1);
	    		    	                   }
	    		    	                    


	    		    	               }
	    		    	               catch(Exception e1)
	    		    	               {
	    		    	                   System.out.println("Error in getData"+e1);
	    		    	               }
	    	    	           	    users.dispose();
	    	        			}
	    	        		});

	    	        		btnNewButton.setBounds(327, 104, 89, 23);
	    	        		panel2.add(btnNewButton);
	    	        		users.setVisible(true);

	    	                   
	    	                }
	    	        } 
	    	 }
	     });

	     
	}


	

}
