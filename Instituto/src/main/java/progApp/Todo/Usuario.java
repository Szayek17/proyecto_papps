package progApp.Todo;

import java.util.Date;

import javax.persistence.*;

@Entity(name = "usuario")
@Table(name = "usuario")
@Inheritance(
    strategy = InheritanceType.JOINED
)
public class Usuario {
	@Column(name = "nickname")
	String nickname;
	@Column(name = "nombre")
	String nombre;
	@Column(name = "apellido")
	String apellido;
	@Id
	@Column(name = "correo")
	String correo;
	@Column(name = "fecha")
	Date fecha;
	
	public Usuario() {
		
	}

	public Usuario(String nickname, String nombre, String apellido, String correo, Date fecha) {
		super();
		this.nickname = nickname;
		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
		this.fecha = fecha;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}



	
	

}
