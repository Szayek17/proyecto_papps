package progApp.Todo;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JScrollPane;

public class VerInstitutos extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
    Connection con=null;
    PreparedStatement ps=null;
    ResultSet rs=null;
    ArrayList<String> myList = new ArrayList<>();


	/**
	 * Launch the application.
	 */
    
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VerInstitutos frame = new VerInstitutos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void Dbconnection()
    {
       try{

               Class.forName("com.mysql.jdbc.Driver");
               con=DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto","root","agusabE123@#");            }
           catch(Exception e)
           {
               System.out.println("Error in connection"+e);
           }
   }
	

	/**
	 * Create the frame.
	 */
	public VerInstitutos() {
		setTitle("Institutos Ingresados");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		@SuppressWarnings("rawtypes")
		JList<?> list = new JList();
		
		
		
	
		JButton btnSalir = new JButton("Salir");
		btnSalir.setFont(new Font("Trebuchet MS", Font.BOLD, 11));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
			}
		});
		contentPane.add(btnSalir, BorderLayout.SOUTH);
		Dbconnection();
	    try{
	    	 ps=con.prepareStatement("select * from instituto");
              
              rs=ps.executeQuery();
              while(rs.next())
              {
                  String listado = (rs.getString(1)); //here you can get data, the '1' indicates column number based on your query
                  myList.add(listado);
              }

        }
        catch(Exception e)
        {
            System.out.println("Error in getData"+e);
        }
	     list = new JList<Object>(myList.toArray());
	     list.setBorder(new LineBorder(new Color(0, 0, 0)));
	     list.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	     scrollPane.setViewportView(list);
	     
	}
		

}
