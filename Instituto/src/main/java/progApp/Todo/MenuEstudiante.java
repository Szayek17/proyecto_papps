package progApp.Todo;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;



import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;

public class MenuEstudiante extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
    Connection con=null;
    PreparedStatement ps=null;
    ResultSet rs=null;
    private static EntityManager manager;
    private static EntityManagerFactory emf;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuEstudiante frame = new MenuEstudiante();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuEstudiante() {
        emf = Persistence.createEntityManagerFactory("Institutos");
        manager = emf.createEntityManager();
		setTitle("Registrar Estudiante");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 264);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(167, 38, 247, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(167, 69, 247, 20);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(167, 100, 247, 20);
		panel.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(167, 131, 247, 20);
		panel.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(167, 162, 247, 20);
		panel.add(textField_4);
		
		JLabel lblNewLabel = new JLabel("Usuario");
		lblNewLabel.setBounds(10, 41, 94, 14);
		panel.add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 72, 115, 14);
		panel.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(10, 103, 127, 14);
		panel.add(lblApellido);
		
		JLabel lblCorreoElectronico = new JLabel("Correo Electronico");
		lblCorreoElectronico.setBounds(10, 134, 147, 14);
		panel.add(lblCorreoElectronico);
		
		JLabel lblFecha = new JLabel("Fecha (Ej. 2020/05/28)");
		lblFecha.setBounds(10, 165, 127, 14);
		panel.add(lblFecha);
		
		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SimpleDateFormat Datefor = new SimpleDateFormat("yyyy/MM/dd");
				try {
					Date utilDate = Datefor.parse(textField_4.getText());
					Usuario registrado = new Usuario(textField.getText(), textField_1.getText(), textField_2.getText(), textField_3.getText(), utilDate);
					if (manager.find(Usuario.class, registrado.getCorreo()) == null) {
						Query query = manager.createQuery("INSERT INTO estudiante (nickname, nombre, apellido, correo, fecha) VALUES (?1, ?2, ?3, ?4, ?5)");
						manager.getTransaction().begin();
						query.setParameter(1, textField.getText());
						query.setParameter(2, textField_1.getText());
						query.setParameter(3, textField_2.getText());
						query.setParameter(4, textField_3.getText());
						query.setParameter(5, utilDate);
						query.executeUpdate();
						manager.getTransaction().commit();
	                    manager.getTransaction().begin();
	                    manager.merge(registrado);
	                    manager.getTransaction().commit();
	                    JOptionPane.showMessageDialog((Component) e.getSource(),
	                        "Estudiante registrado correctamente",
	                        "Operacion Exitosa",
	                        JOptionPane.INFORMATION_MESSAGE);
	                    textField_2.setText("");        
	                    textField_3.setText("");
	                    textField_4.setText("");
	                    textField_1.setText("");
	                    textField.setText("");

	                } else {
	                	JOptionPane.showMessageDialog((Component) e.getSource(),
	                            "Estudiante ya registrado",
	                            "ERROR",
	                            JOptionPane.INFORMATION_MESSAGE);
	                }
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(325, 193, 89, 23);
		panel.add(btnNewButton);
		
		JLabel lblNewLabel_1 = new JLabel("Registro de Estudiantes");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblNewLabel_1.setBounds(167, 0, 247, 27);
		panel.add(lblNewLabel_1);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(226, 193, 89, 23);
		panel.add(btnCancelar);
	}
}
