package progApp.Todo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name = "estudiante")
@Table(name = "estudiante")
public class Estudiante extends Usuario {
	public Estudiante() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Estudiante(String nickname, String nombre, String apellido, String correo, Date fecha) {
		super(nickname, nombre, apellido, correo, fecha);
		this.nickname = nickname;
		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
		this.fecha = fecha;
		
		// TODO Auto-generated constructor stub
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
}
