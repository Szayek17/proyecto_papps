package progApp.Todo;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JOptionPane;

import java.awt.Choice;
import java.awt.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.awt.event.ActionEvent;

public class AltaCurso extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	Connection con=null;
    PreparedStatement ps=null;
    ResultSet rs=null;
    private static EntityManager manager;
    private static EntityManagerFactory emf;
    private JTextField textField;
    private JTextField textURL;
    private JTextField textDescripcion;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AltaCurso frame = new AltaCurso();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AltaCurso() {
		emf = Persistence.createEntityManagerFactory("Institutos");
		manager = emf.createEntityManager();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 480, 444);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		lblNewLabel_1.setBounds(46, 94, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		textField_1 = new JTextField();
		textField_1.setBounds(102, 91, 214, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNewLabel_1_1 = new JLabel("Cantidad Horas:");
		lblNewLabel_1_1.setBounds(46, 159, 89, 14);
		contentPane.add(lblNewLabel_1_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(133, 156, 183, 20);
		contentPane.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(102, 119, 214, 20);
		contentPane.add(textField_3);
		
		JLabel lblDuracion = new JLabel("Duracion:");
		lblDuracion.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblDuracion.setBounds(46, 122, 70, 14);
		contentPane.add(lblDuracion);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Creditos:");
		lblNewLabel_1_1_1.setBounds(46, 187, 46, 14);
		contentPane.add(lblNewLabel_1_1_1);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(102, 184, 214, 20);
		contentPane.add(textField_4);
		
		JLabel lblNewLabel_1_1_2 = new JLabel("Fecha:");
		lblNewLabel_1_1_2.setBounds(46, 219, 46, 14);
		contentPane.add(lblNewLabel_1_1_2);
		
		
		JLabel lblNewLabel_1_1_1_1_3 = new JLabel("Dia");
		lblNewLabel_1_1_1_1_3.setBounds(98, 219, 29, 14);
		contentPane.add(lblNewLabel_1_1_1_1_3);
		
		Choice choice_2 = new Choice();
		choice_2.setBounds(133, 219, 43, 20);
		choice_2.add("01");
        choice_2.add("02");
        choice_2.add("03");
        choice_2.add("04");
        choice_2.add("05");
        choice_2.add("06");
        choice_2.add("07");
        choice_2.add("08");
        choice_2.add("09");
        choice_2.add("10");
        choice_2.add("11");
        choice_2.add("12");
        choice_2.add("13");
        choice_2.add("14");
        choice_2.add("15");
        choice_2.add("16");
        choice_2.add("17");
        choice_2.add("18");
        choice_2.add("19");
        choice_2.add("20");
        choice_2.add("21");
        choice_2.add("22");
        choice_2.add("23");
        choice_2.add("24");
        choice_2.add("25");
        choice_2.add("26");
        choice_2.add("27");
        choice_2.add("28");
        choice_2.add("29");
        choice_2.add("30");
        choice_2.add("31");
		contentPane.add(choice_2);
		
		JLabel lblNewLabel_1_1_1_1_1_1 = new JLabel("Mes");
		lblNewLabel_1_1_1_1_1_1.setBounds(193, 219, 25, 14);
		contentPane.add(lblNewLabel_1_1_1_1_1_1);
		
		Choice choice_1_2 = new Choice();
		choice_1_2.setBounds(224, 219, 43, 20);
		choice_1_2.add("01");
        choice_1_2.add("02");   
        choice_1_2.add("03");
        choice_1_2.add("04");
        choice_1_2.add("05");
        choice_1_2.add("06");
        choice_1_2.add("07");
        choice_1_2.add("08");
        choice_1_2.add("09");
        choice_1_2.add("10");
        choice_1_2.add("11");
        choice_1_2.add("12");
		contentPane.add(choice_1_2);
		
		JLabel lblNewLabel_1_1_1_1_2_1 = new JLabel("A\u00F1o");
		lblNewLabel_1_1_1_1_2_1.setBounds(281, 219, 25, 14);
		contentPane.add(lblNewLabel_1_1_1_1_2_1);
		
		Choice choice_1_1_1 = new Choice();
		choice_1_1_1.setBounds(321, 219, 73, 20);
		choice_1_1_1.add("2019");
        choice_1_1_1.add("2020");
        choice_1_1_1.add("2021");  
        choice_1_1_1.add("2021");
        choice_1_1_1.add("2022");
        choice_1_1_1.add("2023");
        choice_1_1_1.add("2024");
		contentPane.add(choice_1_1_1);
		
		JLabel lblNewLabel_2 = new JLabel("Alta Curso");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_2.setBounds(148, 11, 137, 14);
		contentPane.add(lblNewLabel_2);
		
		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String dia = choice_2.getSelectedItem();
            	String mes = choice_1_2.getSelectedItem();
            	String anio = choice_1_1_1.getSelectedItem();
            	String URL, descripcion;
            	String fec = anio.concat("-");
            	String fec2 = fec.concat(mes);
            	String fec3 = fec2.concat("-");
            	String fec4 = fec3.concat(dia);
            	String duracion = (textField_3.getText());
            	Integer horas = Integer.parseInt(textField_2.getText());
            	Integer creditos = Integer.parseInt(textField_4.getText());
            	LocalDate utilDate = LocalDate.parse(fec4);
				//Date utilDate = Datefor.parse(fec4);
				//Curso registrado = new Curso(textField_1.getText(),textDescripcion.getText(), textField_3.getText(), horas, creditos, utilDate, textURL.getText());
				Curso registrado = new Curso(textField_1.getText(),textDescripcion.getText(), duracion, horas, creditos, utilDate, textURL.getText());
				
				InstitutoBrindaCurso regBrindaCurso = new InstitutoBrindaCurso(textField.getText(), textField_1.getText());
				if(manager.find(Curso.class, registrado.getNombre()) == null) {
					manager.getTransaction().begin();
					manager.merge(registrado);
					manager.getTransaction().commit();
					manager.getTransaction().begin();
					manager.merge(regBrindaCurso);
					manager.getTransaction().commit();
					JOptionPane.showMessageDialog((Component) e.getSource(),
				            "Curso registrado correctamente",
				            "Operacion Exitosa",
				            JOptionPane.INFORMATION_MESSAGE);
				        textField_2.setText("");        
				        textField_3.setText("");
				        textField_4.setText("");
				        textField_1.setText("");
				 } else {
				    	JOptionPane.showMessageDialog((Component) e.getSource(),
				                "Curso ya registrado",
				                "ERROR",
				                JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		
		btnNewButton.setBounds(326, 371, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnSalir.setBounds(227, 371, 89, 23);
		contentPane.add(btnSalir);
		
		JLabel lblNewLabel_1_2 = new JLabel("Instituto:");
		lblNewLabel_1_2.setBounds(46, 63, 46, 14);
		contentPane.add(lblNewLabel_1_2);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(102, 60, 214, 20);
		contentPane.add(textField);
		
		textURL = new JTextField();
		textURL.setBounds(102, 256, 214, 20);
		contentPane.add(textURL);
		textURL.setColumns(10);
		
		JLabel lblURL = new JLabel("URL:");
		lblURL.setBounds(46, 259, 46, 14);
		contentPane.add(lblURL);
		
		textDescripcion = new JTextField();
		textDescripcion.setBounds(102, 287, 214, 73);
		contentPane.add(textDescripcion);
		textDescripcion.setColumns(10);
		
		JLabel lblDescr = new JLabel("Descripci\u00F3n:");
		lblDescr.setBounds(33, 287, 59, 14);
		contentPane.add(lblDescr);
	}
}
