package progApp.Todo;

import java.awt.Component;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import java.awt.Color;
import javax.swing.SpinnerNumberModel;
import javax.swing.JLabel;
import java.awt.Choice;

public class AgregarEdicionCurso extends JFrame {

	private JPanel contentPane;
	private static EntityManager manager;
	private static EntityManagerFactory emf;
	private static final long serialVersionUID = 1L;
	protected static final Component Componet = null;
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	ArrayList<String> myList = new ArrayList<String>();
	@SuppressWarnings("rawtypes")
	public static JList list;
	
	private JTextField tfNombre;
	private JTextArea txtDocentes;
	private JTextField tfDocentes;
	private JButton btnCancelar;
	private JButton bteDarAlta;

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AgregarEdicionCurso frame = new AgregarEdicionCurso(" ");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void Dbconnection() {
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto","root","agusabE123@#");
		} catch (Exception e) {
		}
	}

	/**
	 * Create the frame.
	 */

	public AgregarEdicionCurso(String nom) {
		emf = Persistence.createEntityManagerFactory("Institutos");
		manager = emf.createEntityManager();
		String curso = nom;
		edicionCurso edCurso = new edicionCurso();
		DocenteDaEdCurso dEdCurso = new DocenteDaEdCurso();
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 554, 341);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTextArea txtNombre = new JTextArea();
		txtNombre.setText("Nombre");
		txtNombre.setEnabled(false);
		txtNombre.setBackground(UIManager.getColor("Button.background"));
		txtNombre.setBounds(158, 42, 87, 22);
		contentPane.add(txtNombre);

		tfNombre = new JTextField();
		tfNombre.setBackground(UIManager.getColor("Button.highlight"));
		tfNombre.setBounds(284, 42, 96, 19);
		contentPane.add(tfNombre);
		tfNombre.setColumns(10);

		txtDocentes = new JTextArea();
		txtDocentes.setForeground(Color.BLACK);
		txtDocentes.setText("Docentes");
		txtDocentes.setEnabled(false);
		txtDocentes.setBackground(SystemColor.menu);
		txtDocentes.setBounds(158, 214, 96, 22);
		contentPane.add(txtDocentes);

		tfDocentes = new JTextField();
		tfDocentes.setColumns(10);
		tfDocentes.setBackground(UIManager.getColor("Button.highlight"));
		tfDocentes.setBounds(284, 214, 96, 19);
		contentPane.add(tfDocentes);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(35, 260, 85, 21);
		contentPane.add(btnCancelar);
		
		Choice choice = new Choice();
		choice.setBounds(267, 70, 43, 20);
		choice.add("01");
        choice.add("02");
        choice.add("03");
        choice.add("04");
        choice.add("05");
        choice.add("06");
        choice.add("07");
        choice.add("08");
        choice.add("09");
        choice.add("10");
        choice.add("11");
        choice.add("12");
        choice.add("13");
        choice.add("14");
        choice.add("15");
        choice.add("16");
        choice.add("17");
        choice.add("18");
        choice.add("19");
        choice.add("20");
        choice.add("21");
        choice.add("22");
        choice.add("23");
        choice.add("24");
        choice.add("25");
        choice.add("26");
        choice.add("27");
        choice.add("28");
        choice.add("29");
        choice.add("30");
        choice.add("31");
		contentPane.add(choice);
		
		Choice choice_2 = new Choice();
		choice_2.setBounds(267, 136, 43, 20);
		choice_2.add("01");
        choice_2.add("02");
        choice_2.add("03");
        choice_2.add("04");
        choice_2.add("05");
        choice_2.add("06");
        choice_2.add("07");
        choice_2.add("08");
        choice_2.add("09");
        choice_2.add("10");
        choice_2.add("11");
        choice_2.add("12");
        choice_2.add("13");
        choice_2.add("14");
        choice_2.add("15");
        choice_2.add("16");
        choice_2.add("17");
        choice_2.add("18");
        choice_2.add("19");
        choice_2.add("20");
        choice_2.add("21");
        choice_2.add("22");
        choice_2.add("23");
        choice_2.add("24");
        choice_2.add("25");
        choice_2.add("26");
        choice_2.add("27");
        choice_2.add("28");
        choice_2.add("29");
        choice_2.add("30");
        choice_2.add("31");
		contentPane.add(choice_2);
		
		JLabel lblNewLabel_1_1_1_1_1 = new JLabel("Mes");
		lblNewLabel_1_1_1_1_1.setBounds(327, 70, 25, 14);
		contentPane.add(lblNewLabel_1_1_1_1_1);
		
		JLabel lblNewLabel_1_1_1_1_1_1 = new JLabel("Mes");
		lblNewLabel_1_1_1_1_1_1.setBounds(327, 136, 25, 14);
		contentPane.add(lblNewLabel_1_1_1_1_1_1);
		
		Choice choice_1 = new Choice();
		choice_1.setBounds(358, 70, 43, 20);
		choice_1.add("01");
        choice_1.add("02");   
        choice_1.add("03");
        choice_1.add("04");
        choice_1.add("05");
        choice_1.add("06");
        choice_1.add("07");
        choice_1.add("08");
        choice_1.add("09");
        choice_1.add("10");
        choice_1.add("11");
        choice_1.add("12");
		contentPane.add(choice_1);
		
		Choice choice_1_2 = new Choice();
		choice_1_2.setBounds(358, 136, 43, 20);
		choice_1_2.add("01");
		choice_1_2.add("02");
		choice_1_2.add("03");
		choice_1_2.add("04");
		choice_1_2.add("05");
		choice_1_2.add("06");
		choice_1_2.add("07");
		choice_1_2.add("08");
		choice_1_2.add("09");
		choice_1_2.add("10");
		choice_1_2.add("11");
		choice_1_2.add("12");
		contentPane.add(choice_1_2);
		
		JLabel lblNewLabel_1_1_1_1_2 = new JLabel("A\u00F1o");
		lblNewLabel_1_1_1_1_2.setBounds(415, 70, 25, 14);
		contentPane.add(lblNewLabel_1_1_1_1_2);
		
		JLabel lblNewLabel_1_1_1_1_2_1 = new JLabel("A\u00F1o");
		lblNewLabel_1_1_1_1_2_1.setBounds(415, 136, 25, 14);
		contentPane.add(lblNewLabel_1_1_1_1_2_1);
		
		Choice choice_1_1_1 = new Choice();
		choice_1_1_1.setBounds(455, 136, 73, 20);
		choice_1_1_1.add("2019");
        choice_1_1_1.add("2020");
        choice_1_1_1.add("2021");
        choice_1_1_1.add("2022");
        choice_1_1_1.add("2023");
        choice_1_1_1.add("2024");
		contentPane.add(choice_1_1_1);
		
		Choice choice_1_1 = new Choice();
		choice_1_1.setBounds(455, 70, 73, 20);
		choice_1_1.add("2019");
        choice_1_1.add("2020");
        choice_1_1.add("2021");  
        choice_1_1.add("2021");
        choice_1_1.add("2022");
        choice_1_1.add("2023");
        choice_1_1.add("2024");
		contentPane.add(choice_1_1);


		final JSpinner spCupos = new JSpinner();
		spCupos.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spCupos.setBounds(284, 173, 96, 20);
		contentPane.add(spCupos);

		JTextArea txtCupos = new JTextArea();
		txtCupos.setEnabled(false);
		txtCupos.setText("Cupos");
		txtCupos.setBackground(SystemColor.menu);
		txtCupos.setBounds(158, 170, 59, 22);
		contentPane.add(txtCupos);

		bteDarAlta = new JButton("Dar alta");
		bteDarAlta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				boolean opcion = false;
				String dia = choice.getSelectedItem();
            	String mes = choice_1.getSelectedItem();
            	String anio = choice_1_1.getSelectedItem();
            	String diaF = choice_2.getSelectedItem();
            	String mesF = choice_1_2.getSelectedItem();
            	String anioF = choice_1_1_1.getSelectedItem();
            	String fec = anio.concat("-");
            	String fec2 = fec.concat(mes);
            	String fec3 = fec2.concat("-");
            	String fec4 = fec3.concat(dia);
            	String fecF1 = anioF.concat("-");
            	String fecF2 = fecF1.concat(mesF);
            	String fecF3 = fecF2.concat("-");
            	String fecF = fecF3.concat(diaF);
            	SimpleDateFormat Datefor = new SimpleDateFormat("yyyy-MM-dd"); 
            	LocalDate utilDate = LocalDate.parse(fec4);   
				LocalDate utilDateF = LocalDate.parse(fecF);
if (tfNombre.getText().equals("") && utilDate == null && utilDateF == null
					&& tfDocentes.getText().equals("")) {
				JOptionPane.showMessageDialog((Component) e.getSource(), "Tienen que ingresar datos validos",
						"ERROR", JOptionPane.INFORMATION_MESSAGE);
} else {
				while (opcion != true) {
					
					Dbconnection();
					try {
						ps = con.prepareStatement("select count(nomEdicion) from edicioncurso;");
						rs = ps.executeQuery();
						while (rs.next()) {
							String listado = (rs.getString(1));
							edCurso.setNomEdicion(listado);
						}
						
						con.close();
					} catch (Exception e1) {
						System.out.println("Error en obtener los datos" + e1);
					}
					
					edCurso.setNomCurso(tfNombre.getText());
					edCurso.setNomCurso(curso);
					edCurso.setFechaPublicado(java.time.LocalDate.now());
					edCurso.setFechaIni(utilDate);
					edCurso.setFechaFin(utilDateF);
					if(spCupos.getValue().hashCode() == 0) {
						edCurso.setCupos(-1);
					}else {
						edCurso.setCupos(spCupos.getValue().hashCode());
					}
					dEdCurso.setRefDocente(tfDocentes.getText());
					dEdCurso.setRefEdicion(tfNombre.getText());
					
					

					if (manager.find(edicionCurso.class, edCurso.getNomEdicion()) == null) {
						manager.getTransaction().begin();
						manager.persist(edCurso);
						manager.persist(dEdCurso);
						manager.getTransaction().commit();

						JOptionPane.showMessageDialog((Component) e.getSource(),
								"Edicon de Curso registrada correctamente", "Operacion Exitosa",
								JOptionPane.INFORMATION_MESSAGE);
						tfNombre.setText("");
						opcion = true;

					} else {
						int res = JOptionPane.showOptionDialog(new JFrame(),
								"Desea modificar el nombre de la Edicon de Curso?",
								"Edicion de Curso ya registrada", JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE, null, new Object[] { "Si", "No" },
								JOptionPane.YES_OPTION);
						if (res == JOptionPane.YES_OPTION) {
							
							Dbconnection();
							try {
								ps = con.prepareStatement("select count(nomEdicion) from edicioncurso;");
								rs = ps.executeQuery();
								//edCurso.setNombreCurso(rs.getInt(1));
								con.close();

							} catch (Exception e1) {
								System.out.println("Error en obtener los datos" + e1);
							}
							
							edCurso.setNomEdicion(JOptionPane.showInputDialog(new JFrame(),
									"Ingrese nuevo nombre de Edicion de Curso", null));
//								edCurso.setCurso(curso);
//								edCurso.setFecha_publicado(fechaActual());
//								edCurso.setCupos(spCupos.getValue().hashCode());
//								dEdCurso.setRefDocente(tfDocentes.getText());
//								dEdCurso.setRefEdicion(tfNombre.getText());
						} else if (res == JOptionPane.NO_OPTION) {
							tfNombre.setText("");
						} else if (res == JOptionPane.CLOSED_OPTION) {
							tfNombre.setText("");
						}

					}
				}

}
dispose();
			}
		});
		bteDarAlta.setBounds(414, 260, 85, 21);
		contentPane.add(bteDarAlta);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Fecha Inicio: ");
		lblNewLabel_1_1_1.setBounds(152, 70, 73, 14);
		contentPane.add(lblNewLabel_1_1_1);
		
		JLabel lblNewLabel_1_1_1_2 = new JLabel("Fecha Fin:");
		lblNewLabel_1_1_1_2.setBounds(152, 136, 73, 14);
		contentPane.add(lblNewLabel_1_1_1_2);
		
		JLabel lblNewLabel_1_1_1_1 = new JLabel("Dia");
		lblNewLabel_1_1_1_1.setBounds(232, 70, 29, 14);
		contentPane.add(lblNewLabel_1_1_1_1);
		
		JLabel lblNewLabel_1_1_1_1_3 = new JLabel("Dia");
		lblNewLabel_1_1_1_1_3.setBounds(232, 136, 29, 14);
		contentPane.add(lblNewLabel_1_1_1_1_3);
		
		
	}



	public java.util.Date fechaActual() {
		Calendar c = Calendar.getInstance();
		String dia = Integer.toString(c.get(Calendar.DATE));
		String mes = Integer.toString(c.get(Calendar.MONTH) + 1);
		String anio = Integer.toString(c.get(Calendar.YEAR));
		String fActual = anio + "-" + mes + "-" + dia;
		Date fecha = null;
		SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
		try {
			fecha = formatoFecha.parse(fActual);
			return fecha;
		} catch (ParseException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public boolean buscarNombreEdicionCurso(edicionCurso e) {
		edicionCurso ec = e;
		Dbconnection();

		try {
			ps = con.prepareStatement("select nomCurso from ediconcurso where nombre = '" + ec.getNomEdicion() + "';");
			rs = ps.executeQuery();
			while (rs.next()) {
				String listado = (rs.getString(1));
				if (listado == ec.getNomEdicion()) {
					return false;
				} else {
					return true;
				}
			}
			con.close();
		} catch (Exception ef) {
			System.out.println("Error en obtener los datos" + ef);
		}
		return false;
	}

	public boolean compararFechas(Date fecha) {

		if (fecha.toString().equals("")) {
			return false;
		} else {
			if (fecha.compareTo(fechaActual()) > 0) {
				return true;
			}
		}
		return false;
	}
}
