package progApp.Todo;

//import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;


public class VerFormaciones extends JFrame {

	private JPanel contentPane;
	private static final long serialVersionUID = 1L;
    Connection con=null;
    PreparedStatement ps=null;
    ResultSet rs=null;
    ArrayList<String> myList = new ArrayList<>();
    @SuppressWarnings("rawtypes")
	JList<?> list = new JList();


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VerFormaciones frame = new VerFormaciones();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void DbConnection(){
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto","root","agusabE123@#");
			
		}catch(Exception e){ System.out.println(e);}  
	}

	/**
	 * Create the frame.
	 */
	public VerFormaciones() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		@SuppressWarnings("rawtypes")
		JList<?> list = new JList();
		
		JButton btnNewButton = new JButton("Salir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
			}
			
		});
		btnNewButton.setBounds(0, 238, 434, 23);
		contentPane.add(btnNewButton);
		DbConnection();
		try {
			
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from programaformacion");
			while(rs.next()) {
				String prueba = (rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3));
				myList.add(prueba);
			}
			
		}
		catch(Exception e)
        {
            System.out.println("Error in getData"+e);
        }
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 434, 239);
		contentPane.add(scrollPane);
		
		list = new JList<Object>(myList.toArray());
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    list.setBorder(new LineBorder(new Color(0, 0, 0)));
	    list.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		scrollPane.setViewportView(list);
	}

}
