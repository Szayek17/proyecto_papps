package progApp.Todo;

import java.util.List;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import controladores.cursoController;

import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IF_ElegirInstituto extends JInternalFrame {

	private static final long serialVersionUID = 1L;
	private int cancelo = -1;
	private static cursoController controlador = new cursoController();
	
	public IF_ElegirInstituto(List<Instituto> ins) {
		setTitle("Consulta de curso");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 440, 268);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(281, 215, 104, 25);
		panel.add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(48, 215, 117, 25);
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cancelo=1;
				cerrar();
			}
		});
		panel.add(btnCancelar);
		
		JComboBox<String> listaInstitutos = new JComboBox<String>();
		listaInstitutos.setBounds(130, 125, 179, 24);
		panel.add(listaInstitutos);
		
		JLabel lblSeleccioneElInstituto = new JLabel("Seleccione el instituto al que pertence el curso:");
		lblSeleccioneElInstituto.setBounds(48, 54, 349, 15);
		panel.add(lblSeleccioneElInstituto);
		
		if(ins.size()>0) {
			for(Instituto i : ins)
				listaInstitutos.addItem(i.getNombreInstituto());
			
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					//Elegir cursos.
					
					IF_ElegirCurso ventana2 = new IF_ElegirCurso(
							controlador.listarNombreCursos((String)listaInstitutos.getSelectedItem()));
					ventana2.setSize(450, 300);
					ventana2.setLocation(150, 176);
					ConsultaCurso.escritorio.add(ventana2);
					cerrar();
					ventana2.setVisible(true);
					cancelo=0;
				}
			});
		}
		else {
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					cancelo=1;
					cerrar();
				}
			});
			listaInstitutos.addItem("-Nada para mostrar-");
			JOptionPane.showMessageDialog(null, "No hay institutos registrados en el sistema.");
			cancelo=1;
			cerrar();
		}
	}
	
	private void cerrar() {
		dispose();
	}
	
	public int getCancelo() {
		return cancelo;
	}
}
