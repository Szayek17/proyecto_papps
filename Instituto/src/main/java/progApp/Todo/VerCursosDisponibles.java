package progApp.Todo;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class VerCursosDisponibles extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private final JButton btnSalir = new JButton("Salir");
	private String nombre;
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	ArrayList<String> myList = new ArrayList<>();
	AgregarEdicionCurso aec;
	private JButton btnSiguiente;

	/**
	 * Launch the application.
	 */
	private static VerCursosDisponibles frame = new VerCursosDisponibles(" ");
	
	private JList<?> lstCursos;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void Dbconnection() {
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto","root","agusabE123@#");
		} catch (Exception e) {
			System.out.println("Error in connection" + e);
		}
	}
	
	

	/**
	 * Create the frame.
	 */

	public VerCursosDisponibles(String nom) {
		nombre = nom;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(9, 11, 426, 232);
		contentPane.add(scrollPane);
		btnSalir.setBounds(9, 243, 182, 21);

		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					con.close();
					dispose();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}
		});
		contentPane.add(btnSalir);
		Dbconnection();
		try {
			ps = con.prepareStatement("select nombreCurso from institutobrindacurso where nombreInst = '" + nombre + "';");
			rs = ps.executeQuery();
			while (rs.next()) {
				String listado = (rs.getString(1));
				myList.add(listado);
			}
			con.close();

		} catch (Exception e) {
			System.out.println("Error en obtener los datos" + e);
		}
		
		lstCursos = new JList<Object>(myList.toArray());
		lstCursos.setBorder(new LineBorder(new Color(0, 0, 0)));
		lstCursos.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		scrollPane.setViewportView(lstCursos);
		
		btnSiguiente = new JButton("Siguiente");
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String curso = lstCursos.getSelectedValue().toString();
				aec = new AgregarEdicionCurso(curso);
				aec.setVisible(true);
				dispose();
			}
		});
		btnSiguiente.setBounds(253, 243, 182, 21);
		contentPane.add(btnSiguiente);
	}

}
