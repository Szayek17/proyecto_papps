package progApp.Todo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "docentedaedcurso")
public class DocenteDaEdCurso implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "refDocente")
	private String refDocente;
	
//	@Id
	@Column(name = "refEdCurso")
	private String refEdicion;

	@Override
	public String toString() {
		return "DocenteDaEdCurso [refDocente=" + refDocente + ", refEdicion=" + refEdicion + "]";
	}

	public String getRefDocente() {
		return refDocente;
	}

	public void setRefDocente(String refDocente) {
		this.refDocente = refDocente;
	}

	public String getRefEdicion() {
		return refEdicion;
	}

	public void setRefEdicion(String refEdicion) {
		this.refEdicion = refEdicion;
	}

	
}
