package progApp.Todo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cursoPrevias")
public class cursoPrevias {
	@Id
	@Column(name="nomCurso")
	private String nomCurso;
	
	@Column(name="nomPrevia")
	private String nomPrevia;
	
	public cursoPrevias(String nomCurso, String nomPrevia) {
		this.nomCurso = nomCurso;
		this.nomPrevia = nomPrevia;
	}

	public cursoPrevias() {
	}

	public String getNomCurso() {
		return nomCurso;
	}

	public void setNomCurso(String nomCurso) {
		this.nomCurso = nomCurso;
	}

	public String getNomPrevia() {
		return nomPrevia;
	}

	public void setNomPrevia(String nomPrevia) {
		this.nomPrevia = nomPrevia;
	}
	
	
	
	
}
