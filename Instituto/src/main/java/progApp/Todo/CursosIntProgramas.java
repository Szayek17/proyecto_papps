package progApp.Todo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cursosintprogramas")
public class CursosIntProgramas {
	
	@Id
	@Column(name = "nombreCurso")
	private String nombreCurso;
	
	@Column(name = "nombreProg")
	private String nombreProg;

	public CursosIntProgramas(String nombreCurso, String nombreProg) {
		super();
		this.nombreCurso = nombreCurso;
		this.nombreProg = nombreProg;
	}

	public CursosIntProgramas() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getNombreCurso() {
		return nombreCurso;
	}

	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}

	public String getNombreProg() {
		return nombreProg;
	}

	public void setNombreProg(String nombreProg) {
		this.nombreProg = nombreProg;
	}
	

}
