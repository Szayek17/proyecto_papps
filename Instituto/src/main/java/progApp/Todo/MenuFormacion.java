package progApp.Todo;

import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Choice;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MenuFormacion extends JFrame {

    /**
     * 
     */
    private static EntityManager manager;
    private static EntityManagerFactory emf;
    private static final long serialVersionUID = 1L;
    Connection con=null;
    PreparedStatement ps=null;
    ResultSet rs=null;
    private JPanel contentPane;
    private JTextField nombreInstituto;
    private JTextField desc;

    /**
     * Launch the application.
     */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuFormacion frame = new MenuFormacion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
    /**
     * Create the frame.
     */
    public MenuFormacion() {
    	emf = Persistence.createEntityManagerFactory("Institutos");
        manager = emf.createEntityManager();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 330);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblNewLabel = new JLabel("Programa de Formacion");
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setFont(new Font("Calibri", Font.BOLD, 14));
        lblNewLabel.setBounds(118, 11, 197, 14);
        contentPane.add(lblNewLabel);

        JLabel lblNewLabel_1 = new JLabel("Nombre");
        lblNewLabel_1.setBounds(27, 45, 139, 14);
        contentPane.add(lblNewLabel_1);

        nombreInstituto = new JTextField();
        nombreInstituto.setBounds(164, 42, 260, 20);
        contentPane.add(nombreInstituto);
        nombreInstituto.setColumns(10);

        JLabel lblNewLabel_1_1 = new JLabel("Descripcion");
        lblNewLabel_1_1.setBounds(27, 79, 139, 14);
        contentPane.add(lblNewLabel_1_1);
        
        desc = new JTextField();
        desc.setColumns(10);
        desc.setBounds(164, 76, 260, 20);
        contentPane.add(desc);
        desc.setColumns(10);
        
        Choice choice = new Choice();
        choice.setBounds(142, 132, 43, 20);
        choice.add("01");
        choice.add("02");
        choice.add("03");
        choice.add("04");
        choice.add("05");
        choice.add("06");
        choice.add("07");
        choice.add("08");
        choice.add("09");
        choice.add("10");
        choice.add("11");
        choice.add("12");
        choice.add("13");
        choice.add("14");
        choice.add("15");
        choice.add("16");
        choice.add("17");
        choice.add("18");
        choice.add("19");
        choice.add("20");
        choice.add("21");
        choice.add("22");
        choice.add("23");
        choice.add("24");
        choice.add("25");
        choice.add("26");
        choice.add("27");
        choice.add("28");
        choice.add("29");
        choice.add("30");
        choice.add("31");
        contentPane.add(choice);
        
        Choice choice_1 = new Choice();
        choice_1.setBounds(233, 132, 43, 20);
        choice_1.add("01");
        choice_1.add("02");   
        choice_1.add("03");
        choice_1.add("04");
        choice_1.add("05");
        choice_1.add("06");
        choice_1.add("07");
        choice_1.add("08");
        choice_1.add("09");
        choice_1.add("10");
        choice_1.add("11");
        choice_1.add("12");
        contentPane.add(choice_1);
        
        Choice choice_1_1 = new Choice();
        choice_1_1.setBounds(330, 132, 73, 20);
        choice_1_1.add("2019");
        choice_1_1.add("2020");
        choice_1_1.add("2021");  
        choice_1_1.add("2021");
        choice_1_1.add("2022");
        choice_1_1.add("2023");
        choice_1_1.add("2024");
        contentPane.add(choice_1_1);
        
        JLabel lblNewLabel_1_1_1 = new JLabel("Fecha Inicio: ");
        lblNewLabel_1_1_1.setBounds(27, 132, 73, 14);
        contentPane.add(lblNewLabel_1_1_1);
        
        JLabel lblNewLabel_1_1_1_1 = new JLabel("Dia");
        lblNewLabel_1_1_1_1.setBounds(107, 132, 29, 14);
        contentPane.add(lblNewLabel_1_1_1_1);
        
        JLabel lblNewLabel_1_1_1_1_1 = new JLabel("Mes");
        lblNewLabel_1_1_1_1_1.setBounds(202, 132, 25, 14);
        contentPane.add(lblNewLabel_1_1_1_1_1);
        
        JLabel lblNewLabel_1_1_1_1_2 = new JLabel("A\u00F1o");
        lblNewLabel_1_1_1_1_2.setBounds(290, 132, 25, 14);
        contentPane.add(lblNewLabel_1_1_1_1_2);
        
        JLabel lblNewLabel_1_1_1_2 = new JLabel("Fecha Fin:");
        lblNewLabel_1_1_1_2.setBounds(27, 198, 73, 14);
        contentPane.add(lblNewLabel_1_1_1_2);
        
        JLabel lblNewLabel_1_1_1_1_3 = new JLabel("Dia");
        lblNewLabel_1_1_1_1_3.setBounds(107, 198, 29, 14);
        contentPane.add(lblNewLabel_1_1_1_1_3);
        
        Choice choice_2 = new Choice();
        choice_2.setBounds(142, 198, 43, 20);
        choice_2.add("01");
        choice_2.add("02");
        choice_2.add("03");
        choice_2.add("04");
        choice_2.add("05");
        choice_2.add("06");
        choice_2.add("07");
        choice_2.add("08");
        choice_2.add("09");
        choice_2.add("10");
        choice_2.add("11");
        choice_2.add("12");
        choice_2.add("13");
        choice_2.add("14");
        choice_2.add("15");
        choice_2.add("16");
        choice_2.add("17");
        choice_2.add("18");
        choice_2.add("19");
        choice_2.add("20");
        choice_2.add("21");
        choice_2.add("22");
        choice_2.add("23");
        choice_2.add("24");
        choice_2.add("25");
        choice_2.add("26");
        choice_2.add("27");
        choice_2.add("28");
        choice_2.add("29");
        choice_2.add("30");
        choice_2.add("31");
        contentPane.add(choice_2);
        
        JLabel lblNewLabel_1_1_1_1_1_1 = new JLabel("Mes");
        lblNewLabel_1_1_1_1_1_1.setBounds(202, 198, 25, 14);
        contentPane.add(lblNewLabel_1_1_1_1_1_1);
        
        Choice choice_1_2 = new Choice();
        choice_1_2.setBounds(233, 198, 43, 20);
        choice_1_2.add("01");
        choice_1_2.add("02");
        choice_1_2.add("03");
        choice_1_2.add("04");
        choice_1_2.add("05");
        choice_1_2.add("06");
        choice_1_2.add("07");
        choice_1_2.add("08");
        choice_1_2.add("09");
        choice_1_2.add("10");
        choice_1_2.add("11");
        choice_1_2.add("12");
        contentPane.add(choice_1_2);
        
        JLabel lblNewLabel_1_1_1_1_2_1 = new JLabel("A\u00F1o");
        lblNewLabel_1_1_1_1_2_1.setBounds(290, 198, 25, 14);
        contentPane.add(lblNewLabel_1_1_1_1_2_1);
        
        Choice choice_1_1_1 = new Choice();
        choice_1_1_1.setBounds(330, 198, 73, 20);
        choice_1_1_1.add("2019");
        choice_1_1_1.add("2020");
        choice_1_1_1.add("2021");
        choice_1_1_1.add("2022");
        choice_1_1_1.add("2023");
        choice_1_1_1.add("2024");
        contentPane.add(choice_1_1_1);
        
        JButton btnAceptar = new JButton("Aceptar");
        btnAceptar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	String dia = choice.getSelectedItem();
            	String mes = choice_1.getSelectedItem();
            	String anio = choice_1_1.getSelectedItem();
            	String diaF = choice_2.getSelectedItem();
            	String mesF = choice_1_2.getSelectedItem();
            	String anioF = choice_1_1_1.getSelectedItem();
            	String fec = anio.concat("/");
            	String fec2 = fec.concat(mes);
            	String fec3 = fec2.concat("/");
            	String fec4 = fec3.concat(dia);
            	String fecF1 = anioF.concat("/");
            	String fecF2 = fecF1.concat(mesF);
            	String fecF3 = fecF2.concat("/");
            	String fecF = fecF3.concat(diaF);
            	SimpleDateFormat Datefor = new SimpleDateFormat("yyyy/MM/dd"); 
            	try {
            		java.util.Date utilDate = Datefor.parse(fec4);   
            		java.util.Date utilDateF = Datefor.parse(fecF);          		
            		Prog_Formacion registrado = new Prog_Formacion(nombreInstituto.getText(), desc.getText(), utilDate, utilDateF);
            		if (nombreInstituto.getText().equals("")) {
                    JOptionPane.showMessageDialog((Component) e.getSource(),
                        "Nombre Invalido",
                        "ERROR",
                        JOptionPane.INFORMATION_MESSAGE);
                } else {
                    if (manager.find(Prog_Formacion.class, registrado.getNombreForm()) == null) {
                        manager.getTransaction().begin();
                        manager.merge(registrado);
                        manager.getTransaction().commit();
                        JOptionPane.showMessageDialog((Component) e.getSource(),
                            "Programa registrado correctamente",
                            "Operacion Exitosa",
                            JOptionPane.INFORMATION_MESSAGE);
                        nombreInstituto.setText("");
                        desc.setText("");
                    

                    } else {
                        int res = JOptionPane.showOptionDialog(new JFrame(), "Desea modificar el nombre del Programa?", "Programa ya registrado",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                            new Object[] {
                                "Si",
                                "No"
                            }, JOptionPane.YES_OPTION);
                        if (res == JOptionPane.YES_OPTION) {
                            System.out.println("Si");
                            String modificado = JOptionPane.showInputDialog(new JFrame(), "Ingrese nuevo nombre de Programa", null);
                            if (modificado.isEmpty() == true) {
                                JOptionPane.showMessageDialog((Component) e.getSource(),
                                    "Nombre Invalido",
                                    "ERROR",
                                    JOptionPane.INFORMATION_MESSAGE);
                            } else {
                                if (manager.find(Prog_Formacion.class, modificado) != null) {
                                    JOptionPane.showMessageDialog((Component) e.getSource(),
                                        "Nombre Invalido",
                                        "ERROR",
                                        JOptionPane.INFORMATION_MESSAGE);
                                } else {
                                    manager.getTransaction().begin();
                                    manager.remove(manager.contains(registrado) ? registrado : manager.merge(registrado));
                                    manager.getTransaction().commit();
                                    registrado = new Prog_Formacion(modificado);
                                    manager.getTransaction().begin();
                                    manager.merge(registrado);
                                    manager.getTransaction().commit();
                                    JOptionPane.showMessageDialog((Component) e.getSource(),
                                        "Programa modificado correctamente",
                                        "Operacion Exitosa",
                                        JOptionPane.INFORMATION_MESSAGE);
                                }
                            }


                            nombreInstituto.setText("");
                            desc.setText("");
                        } else if (res == JOptionPane.NO_OPTION) {
                            nombreInstituto.setText("");
                            desc.setText("");
                        } else if (res == JOptionPane.CLOSED_OPTION) {
                            nombreInstituto.setText("");
                            desc.setText("");
                        }

                    }
                }

            	}catch (ParseException e2) {
    		        e2.printStackTrace();
    		    }
            }
            	
        });
        
        btnAceptar.setBounds(335, 257, 89, 23);
        contentPane.add(btnAceptar);

        JButton btnCancelar = new JButton("Salir");
        btnCancelar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                dispose();
            }
        });
        btnCancelar.setBounds(222, 257, 89, 23);
        contentPane.add(btnCancelar);
        
        
        
       
        

        
    }
}
