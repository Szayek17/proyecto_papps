package progApp.Todo;

//import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;


import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;


import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;


public class SeleccionoFormacion extends JFrame {

	private JPanel contentPane;
	private static EntityManagerFactory emf;
	private static final long serialVersionUID = 1L;
    Connection con=null;
    PreparedStatement ps=null;
    PreparedStatement ps2=null;
    ResultSet rs=null;
    //ResultSet rs2=null;
    ArrayList<String> myList = new ArrayList<>();
    JList<?> listaForm;


	/**
	 * Launch the application.
	 */
    
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SeleccionoFormacion frame = new SeleccionoFormacion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public void DbConnection(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto","root","agusabE123@#");
			
		}catch(Exception e){ System.out.println(e);}  
	}

	/**
	 * Create the frame.
	 */
	public SeleccionoFormacion() {
		
		emf = Persistence.createEntityManagerFactory("Institutos");
        emf.createEntityManager();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		@SuppressWarnings("rawtypes")
		JList<?> list = new JList();
		
		JButton btnNewButton = new JButton("Salir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
			}
			
		});
		btnNewButton.setBounds(0, 238, 211, 23);
		contentPane.add(btnNewButton);
		DbConnection();
		try {
			
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select nombre from programaformacion");
			while(rs.next()) {
				String prueba = (rs.getString(1));
				myList.add(prueba);
			}
			
		}
		catch(Exception e)
        {
            System.out.println("Error in getData"+e);
        }
		
			
			list = new JList<Object>(myList.toArray());
	     list.addMouseListener(new MouseAdapter() {
	    	 public void mouseClicked(MouseEvent evt) {
	    		 JList<?> list = (JList<?>)evt.getSource();
	    	        if (evt.getClickCount() == 1) {

	    	            // Double-click detected
//	    	        	String formacioDeCurso = list.getSelectedValue().toString();
//	    	        	System.out.println(formacioDeCurso);
	    	        	int index = list.locationToIndex(evt.getPoint());
	    	               if (index >= 0) {
	    	            	   VerCursos muestroCursos = new VerCursos();
	    	            	   muestroCursos.setVisible(true);
	    	            	   muestroCursos.setLocationRelativeTo(null);
	    	               }
	    	        	
	    	        	}
	    	        	
	    	        	
	    	        	
	    	            
	    	 }
	    	 
	     });
		
	    	 
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 434, 239);
		contentPane.add(scrollPane);
		
		listaForm = new JList<Object>(myList.toArray());
		listaForm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listaForm.setBorder(new LineBorder(new Color(0, 0, 0)));
		listaForm.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		scrollPane.setViewportView(listaForm);
		
		JButton btnNewButton_1 = new JButton("Aceptar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String form = listaForm.getSelectedValue().toString();
				SeleccionoCurso cursos = new SeleccionoCurso(form);
				cursos.setVisible(true);
				dispose();
			}
		});
		btnNewButton_1.setBounds(221, 238, 213, 23);
		contentPane.add(btnNewButton_1);
	}
}
