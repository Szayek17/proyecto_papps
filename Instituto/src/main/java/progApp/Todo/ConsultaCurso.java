package progApp.Todo;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controladores.institutoController;

import javax.swing.JButton;
import javax.swing.JDesktopPane;

public class ConsultaCurso extends JFrame {
	
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	public static JDesktopPane escritorio;
	private static institutoController controlador = new institutoController(); 

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultaCurso frame = new ConsultaCurso();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public ConsultaCurso() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 760, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		escritorio= new JDesktopPane();
		escritorio.setBounds(0, 0, 760, 620);
		contentPane.add(escritorio);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				escritorio=null;
			}
		});
		btnSalir.setBounds(322, 630, 117, 25);
		contentPane.add(btnSalir);
		
		//Elegir instituto.
		//ElegirInstituto "leerá" listaIns para obtener la lista.
		IF_ElegirInstituto ventana1 = new IF_ElegirInstituto(controlador.listarInstitutos());
		ventana1.setSize(450, 300);
		ventana1.setLocation(150, 176);
		escritorio.add(ventana1);
		ventana1.setVisible(true);
	}
}