package progApp.Todo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "programaformacion")
public class Prog_Formacion {
	
	@Id
	@Column(name = "nombre")
	private String nombreForm;
	
	@Column(name = "descripcion")
	private String desc;
	@Column(name = "fIni")
	@Temporal(TemporalType.DATE)
	private java.util.Date fIni;
	@Column(name = "fFin")
	@Temporal(TemporalType.DATE)
	private java.util.Date fFin;
	
	public Prog_Formacion() {

		// TODO Auto-generated constructor stub
	}
	
	

	public Prog_Formacion(String nombreForm, String desc, Date fIni, Date fFin) {
		super();
		this.nombreForm = nombreForm;
		this.desc = desc;
		this.fIni = fIni;
		this.fFin = fFin;
	}



	public Prog_Formacion(String nombreForm) {
		super();
		this.nombreForm = nombreForm;
	}


	public String getNombreForm() {
		return nombreForm;
	}

	public void setNombreForm(String nombreForm) {
		this.nombreForm = nombreForm;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public java.util.Date getfIni() {
		return fIni;
	}

	public void setfIni(java.util.Date fIni) {
		this.fIni = fIni;
	}

	public java.util.Date getfFin() {
		return fFin;
	}

	public void setfFin(java.util.Date fFin) {
		this.fFin = fFin;
	}
	
	

}
