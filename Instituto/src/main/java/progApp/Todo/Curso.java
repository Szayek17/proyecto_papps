package progApp.Todo;


import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "curso")
public class Curso {
	
	@Id
	@Column(name = "nomCurso")
	private String nombre;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	@Column(name = "duracion")
	private String duracion;
	
	@Column(name = "cantHoras")
	private int cantHoras;
	
	@Column(name = "cantCreditos")
	private int cantCreditos;
	
	@Column(name = "fechaPublicado")
	private LocalDate fechaPublicado;
	
	@Column(name = "url")
	private String url;

	public Curso() {
		
	}
	
	public Curso(String nombre, String descripcion, String duracion, int cantHoras, int cantCreditos,
			LocalDate fechaPublicado, String url) {
		//super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.duracion = duracion;
		this.cantHoras = cantHoras;
		this.cantCreditos = cantCreditos;
		this.fechaPublicado = fechaPublicado;
		this.url = url;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDuracion() {
		return duracion;
	}

	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}

	public int getCantHoras() {
		return cantHoras;
	}

	public void setCantHoras(int cantHoras) {
		this.cantHoras = cantHoras;
	}

	public int getCantCreditos() {
		return cantCreditos;
	}

	public void setCantCreditos(int cantCreditos) {
		this.cantCreditos = cantCreditos;
	}

	public LocalDate getFechaPublicado() {
		return fechaPublicado;
	}

	public void setFechaPublicado(LocalDate fechaPublicado) {
		this.fechaPublicado = fechaPublicado;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	@Override
	public String toString() {
		return "Curso [nombre=" + nombre + ", descripcion=" + descripcion + ", duracion=" + duracion + ", cantHoras="
				+ cantHoras + ", cantCreditos=" + cantCreditos + ", fechaPublicado=" + fechaPublicado + ", url=" + url
				+ "]";
	}
}