package progApp.Todo;
/*
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder; */

import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.UIManager;

public class MenuEdicionCurso extends JFrame {

	private JPanel contentPane;
	private static final long serialVersionUID = 1L;
	protected static final Component Componet = null;
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	VerCursosDisponibles verCurso;
	ArrayList<String> myList = new ArrayList<String>();
	public static JFrame frameList;
	@SuppressWarnings("rawtypes")
	public static JList list;
	private JTextField tfInstituto;

	/**
	 * Launch the application.
	 */
	private static MenuEdicionCurso frame = new MenuEdicionCurso();

	public static void main(String[] args) {


		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void Dbconnection() {
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto","root","agusabE123@#");
		} catch (Exception e) {
			System.out.println("Error en conexion" + e);
		}
	}

	/**
	 * Create the frame.
	 */
	public MenuEdicionCurso() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 520, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTextPane txtpnIngreseInstituto = new JTextPane();
		txtpnIngreseInstituto.setBounds(43, 37, 110, 19);
		txtpnIngreseInstituto.setEnabled(false);
		txtpnIngreseInstituto.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtpnIngreseInstituto.setText("Ingrese instituto");
		txtpnIngreseInstituto.setBackground(UIManager.getColor("Button.background"));
		contentPane.add(txtpnIngreseInstituto);

		tfInstituto = new JTextField();
		tfInstituto.setBounds(176, 37, 177, 19);
		contentPane.add(tfInstituto);
		tfInstituto.setColumns(10);

		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(55, 103, 98, 21);
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnSalir.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contentPane.add(btnSalir);
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(358, 103, 98, 21);
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verCurso = new VerCursosDisponibles(tfInstituto.getText());
				verCurso.setVisible(true);
				
			}
		});
		btnBuscar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contentPane.add(btnBuscar);
		dispose();
	}
	
}
