package progApp.Todo;



//import java.util.Date;


import javax.persistence.Entity;
import javax.persistence.Table;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Id;

@Entity
@Table(name = "edicioncurso")
public class edicionCurso {
	
	@Id
	@Column(name = "nomEdicion")
	private String nomEdicion;
	
	@Column(name = "fechaIni")
	private LocalDate fechaIni;
	
	
	@Column(name = "fechaFin")
	private LocalDate fechaFin;
	

	@Column(name = "fechaPublicado")
	private LocalDate fechaPublicado;
	
	@Column(name = "cupos")
	private int cupos;
	@Column(name = "nomCurso")
	private String nomCurso;
	@Column(name = "estudianteSus")
	private String estudianteSus;
	

	public edicionCurso() {
		
	}


	public String getNomEdicion() {
		return nomEdicion;
	}


	public void setNomEdicion(String nomEdicion) {
		this.nomEdicion = nomEdicion;
	}


	public LocalDate getFechaIni() {
		return fechaIni;
	}


	public void setFechaIni(LocalDate fechaIni) {
		this.fechaIni = fechaIni;
	}


	public LocalDate getFechaFin() {
		return fechaFin;
	}


	public void setFechaFin(LocalDate fechaFin) {
		this.fechaFin = fechaFin;
	}


	public LocalDate getFechaPublicado() {
		return fechaPublicado;
	}


	public void setFechaPublicado(LocalDate fechaPublicado) {
		this.fechaPublicado = fechaPublicado;
	}


	public int getCupos() {
		return cupos;
	}


	public void setCupos(int cupos) {
		this.cupos = cupos;
	}


	public String getNomCurso() {
		return nomCurso;
	}


	public void setNomCurso(String nomCurso) {
		this.nomCurso = nomCurso;
	}


	public String getEstudianteSus() {
		return estudianteSus;
	}


	public void setEstudianteSus(String estudianteSus) {
		this.estudianteSus = estudianteSus;
	}


	public edicionCurso(String nomEdicion, LocalDate fechaIni, LocalDate fechaFin, LocalDate fechaPublicado, int cupos,
			String nomCurso, String estudianteSus) {
		super();
		this.nomEdicion = nomEdicion;
		this.fechaIni = fechaIni;
		this.fechaFin = fechaFin;
		this.fechaPublicado = fechaPublicado;
		this.cupos = cupos;
		this.nomCurso = nomCurso;
		this.estudianteSus = estudianteSus;
	}


	@Override
	public String toString() {
		return "edicionCurso [nomEdicion=" + nomEdicion + ", fechaIni=" + fechaIni + ", fechaFin=" + fechaFin
				+ ", fechaPublicado=" + fechaPublicado + ", cupos=" + cupos + ", nomCurso=" + nomCurso
				+ ", estudianteSus=" + estudianteSus + "]";
	}
	
	

}
