package progApp.Todo;

import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import modelos.manager.Manager;
import vistas.consultaEdicionCurso;
import vistas.inscripcionEdCurso;

import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.SwingConstants;

public class MenuPrincipal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static Manager manag;
    Connection con=null;
    PreparedStatement ps=null;
    ResultSet res=null;
    int rs;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipal frame = new MenuPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public void Dbconnection()
    {
       try{

               Class.forName("com.mysql.jdbc.Driver");
               con=DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto","root","agusabE123@#");            }
           catch(Exception e)
           {
               System.out.println("Error in connection"+e);
           }
   }
	


	
	public MenuPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 704, 491);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		manag = Manager.getInstance();
		JLabel lblNewLabel = new JLabel("Menu Principal");
		lblNewLabel.setBounds(5, 5, 678, 34);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 28));
		contentPane.add(lblNewLabel);
		ClassLoader classLoader = getClass().getClassLoader();
	    JLabel lblNewLabel_2 = new JLabel("");
	    lblNewLabel_2.setBounds(10, 355, 89, 86);
		ImageIcon myimage = new ImageIcon(classLoader.getResource("images/logo_transparent.png"));
        Image img1 = myimage.getImage();
        Image img2 = img1.getScaledInstance(lblNewLabel_2.getWidth(), lblNewLabel_2.getWidth(), Image.SCALE_SMOOTH);
        ImageIcon i = new ImageIcon(img2);
        lblNewLabel_2.setIcon(i);
        contentPane.add(lblNewLabel_2);
		laminaMenu milamina = new laminaMenu();
		milamina.setBounds(5, 44, 678, 408);
		getContentPane().add(milamina);
		setVisible(true);

	}
}

class laminaMenu extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public laminaMenu() {

		JMenuBar mibarra = new JMenuBar();
		JMenu archivo = new JMenu("Institutos");
		JMenu edicion = new JMenu("Usuarios");
		JMenu herramienta = new JMenu("Programa de Formacion");
		JMenu archivo2 = new JMenu("Edicion Curso");
		JMenu edicion2 = new JMenu("Curso");
		JMenu opciones = new JMenu("Placeholder");
		JMenuItem subir = new JMenuItem("Registrar");
		JMenuItem guardarcomo = new JMenuItem("Ver Todos");
		JMenu cortar = new JMenu("Docente");
		JMenu copiar = new JMenu("Estudiante");
		JMenuItem regUser = new JMenuItem("Registrar");
		JMenuItem modUser = new JMenuItem("Modificar");
		JMenuItem regDoc = new JMenuItem("Registrar");
		JMenuItem verUsers = new JMenuItem("Ver Todos");
		JMenuItem generales = new JMenuItem("Crear Programa de Formacion");
		JMenuItem generales2 = new JMenuItem("Ver Programas de Formacion");
		JMenuItem opcion1 = new JMenuItem("Placeholder");
		JMenuItem opcion2 = new JMenuItem("Placeholder");
		JMenuItem opcionConsultaEdCurso = new JMenuItem("Consulta edicion de curso");
		JMenuItem opcionInscripcionEdCurso = new JMenuItem("Inscripción a edición de curso");
		JMenuItem opcionConsultaCurso = new JMenuItem("Consulta de Curso");
		JMenuItem agregarEdiCurso = new JMenuItem("Agregar Edicion de Curso");
		JMenuItem verCur = new JMenuItem("Ver Cursos");
		JMenuItem alta = new JMenuItem("Alta de Curso");
		JMenuItem agregar = new JMenuItem("Agregar Curso a Programa");
		archivo.add(subir);
		archivo.add(guardarcomo);
		edicion.add(copiar);
		edicion.add(cortar);
		edicion.add(verUsers);
		edicion.add(modUser);
		copiar.add(regUser);
		cortar.add(regDoc);
		herramienta.add(generales);
		herramienta.add(generales2);
		herramienta.add(agregar);
		opciones.add(opcion1);
		opciones.add(opcion2);
		edicion2.add(opcionConsultaCurso);
		edicion2.add(verCur);
		edicion2.add(alta);
		mibarra.add(archivo);
		mibarra.add(edicion);
		mibarra.add(herramienta);
		mibarra.add(archivo2);
		mibarra.add(edicion2);
		archivo2.add(opcionConsultaEdCurso);
		archivo2.add(opcionInscripcionEdCurso);
		archivo2.add(agregarEdiCurso);
		gestorMenus suboArchivo = new gestorMenus();
		gestorEstudiantes registroEstudiante = new gestorEstudiantes();
		gestorDocentes registroDocente = new gestorDocentes();
		gestorFormacion registroFormacion = new gestorFormacion();
		gestorVeoFormaciones veoFormaciones = new gestorVeoFormaciones();
		gestorInstitutos veoInst = new gestorInstitutos();
		gestorVeoUsuarios veousr = new gestorVeoUsuarios();
		gestorModUsuarios modUsr = new gestorModUsuarios();
		gestorEdicionCursos edCurs = new gestorEdicionCursos();
		gestorInscripcionEdicionCursos inscEdCurs = new gestorInscripcionEdicionCursos();
		gestorConsultaCurso consCurso = new gestorConsultaCurso();
		gestorAgregarEdiCurso aEdiCurso = new gestorAgregarEdiCurso();
		gestorVerCursos verCurs = new gestorVerCursos();
		gestorAltaCursos altaC = new gestorAltaCursos();
		gestorAgregarCursoAPrograma agregarCursoP = new gestorAgregarCursoAPrograma();
		
		subir.addActionListener(suboArchivo);
		guardarcomo.addActionListener(veoInst);
		regUser.addActionListener(registroEstudiante);
		regDoc.addActionListener(registroDocente);
		verUsers.addActionListener(veousr);
		generales.addActionListener(registroFormacion);
		generales2.addActionListener(veoFormaciones);
		modUser.addActionListener(modUsr);
		opcionConsultaEdCurso.addActionListener(edCurs);
		opcionInscripcionEdCurso.addActionListener(inscEdCurs);
		opcionConsultaCurso.addActionListener(consCurso);
		agregarEdiCurso.addActionListener(aEdiCurso);
		verCur.addActionListener(verCurs);
		alta.addActionListener(altaC);
		agregar.addActionListener(agregarCursoP);

		add(mibarra);


	}
	
	
	private class gestorMenus implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			

			MenuInstituto menuInst = new MenuInstituto();
			menuInst.setVisible(true);
			menuInst.setLocationRelativeTo(null);

		}
		
	}
	
	private class gestorInstitutos implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			

			VerInstitutos verInst = new VerInstitutos();
			verInst.setVisible(true);
			verInst.setLocationRelativeTo(null);
			

		}
		
	}
	
	private class gestorEstudiantes implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			

			MenuEstudiante menuInst = new MenuEstudiante();
			menuInst.setVisible(true);
			menuInst.setLocationRelativeTo(null);
			

		}
		
	}
	
	private class gestorDocentes implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			

			MenuDocente menuDoc = new MenuDocente();
			menuDoc.setVisible(true);
			menuDoc.setLocationRelativeTo(null);

		}
		
	}
	
	private class gestorFormacion implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			MenuFormacion menuFormacion = new MenuFormacion();
			menuFormacion.setVisible(true);
			menuFormacion.setLocationRelativeTo(null);
			
		}

	}
	
	private class gestorVeoFormaciones implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			VerFormaciones menuVerFormaciones = new VerFormaciones();
			menuVerFormaciones.setVisible(true);
			menuVerFormaciones.setLocationRelativeTo(null);
			
		}

	}
	
	private class gestorVeoUsuarios implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			VerUsuarios menuVerUsuarios = new VerUsuarios();
			menuVerUsuarios.setVisible(true);
			menuVerUsuarios.setLocationRelativeTo(null);
			
		}
		
	}
	
	private class gestorModUsuarios implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			ModUsuarios menuModUsuarios = new ModUsuarios();
			menuModUsuarios.setVisible(true);
			menuModUsuarios.setLocationRelativeTo(null);
			
		}
		
	}
	
	private class gestorEdicionCursos implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
		consultaEdicionCurso menuEdicionCursos = new consultaEdicionCurso();
		menuEdicionCursos.setVisible(true);
		menuEdicionCursos.setLocationRelativeTo(null);
			
		}
	}
	
	private class gestorInscripcionEdicionCursos implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
		inscripcionEdCurso menuInscripcionEdCursos = new inscripcionEdCurso();
		
		menuInscripcionEdCursos.setVisible(true);
		menuInscripcionEdCursos.setLocationRelativeTo(null);
			
		}
	}
	
	private class gestorConsultaCurso implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
		ConsultaCurso ventana = new ConsultaCurso();
		
		ventana.setVisible(true);
		ventana.setLocationRelativeTo(null);
			
		}
	}
	
	private class gestorAgregarEdiCurso implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
		MenuEdicionCurso ventana = new MenuEdicionCurso();
		
		ventana.setVisible(true);
		ventana.setLocationRelativeTo(null);
			
		}
	}
	
	private class gestorVerCursos implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
		VerCursos ventana = new VerCursos();
		
		ventana.setVisible(true);
		ventana.setLocationRelativeTo(null);
			
		}
	}
	
	private class gestorAltaCursos implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
		AltaCurso ventana = new AltaCurso();
		
		ventana.setVisible(true);
		ventana.setLocationRelativeTo(null);
			
		}
	}
	
	private class gestorAgregarCursoAPrograma implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
		SeleccionoFormacion ventana = new SeleccionoFormacion();
		
		ventana.setVisible(true);
		ventana.setLocationRelativeTo(null);
			
		}
	}
}
