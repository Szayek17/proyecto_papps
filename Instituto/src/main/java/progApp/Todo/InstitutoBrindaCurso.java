package progApp.Todo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "institutobrindacurso")
public class InstitutoBrindaCurso {
	
	@Column(name = "nombreInst")
	String nombreInst;
	
	@Id
	@Column(name = "nombreCurso")
	String nombreCurso;

	public String getNombreInst() {
		return nombreInst;
	}

	public void setNombreInst(String nombreInst) {
		this.nombreInst = nombreInst;
	}

	public String getNombreCurso() {
		return nombreCurso;
	}

	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}

	public InstitutoBrindaCurso(String nombreInst, String nombreCurso) {
		super();
		this.nombreInst = nombreInst;
		this.nombreCurso = nombreCurso;
	}

	public InstitutoBrindaCurso() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
