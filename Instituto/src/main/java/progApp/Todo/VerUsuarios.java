package progApp.Todo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class VerUsuarios extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
    Connection con=null;
    PreparedStatement ps=null;
    PreparedStatement ps2=null;
    ResultSet rs=null;
    ResultSet rs2=null;
    ArrayList<String> myList = new ArrayList<>();
    private static EntityManagerFactory emf;

	/**
	 * Launch the application.
	 */
    
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VerUsuarios frame = new VerUsuarios();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}



	public void Dbconnection()
    {
       try{

               Class.forName("com.mysql.jdbc.Driver");
               con=DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto","root","agusabE123@#");            }
           catch(Exception e)
           {
               System.out.println("Error in connection"+e);
           }
   }
	/**
	 * Create the frame.
	 */
	public VerUsuarios() {
		setTitle("Usuarios Registrados");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
        emf = Persistence.createEntityManagerFactory("Institutos");
        emf.createEntityManager();
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		JList<?> list = new JList<Object>();
		JButton btnSalir = new JButton("Salir");
		btnSalir.setFont(new Font("Trebuchet MS", Font.BOLD, 11));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
			}
		});
		contentPane.add(btnSalir, BorderLayout.SOUTH);
		Dbconnection();
	    try{
	    	 ps=con.prepareStatement("select * from usuario");
              
              rs=ps.executeQuery();
              while(rs.next())
              {
                  String nombre = (rs.getString(2)); 
                  String apellido = (rs.getString(3));
                  String completo = nombre + " " + apellido;
                  myList.add(completo);
              }

        }
        catch(Exception e)
        {
            System.out.println("Error in getData"+e);
        }
	     list = new JList<Object>(myList.toArray());
	     list.addMouseListener(new MouseAdapter() {
	    	 public void mouseClicked(MouseEvent evt) {
	    		 JList<?> list = (JList<?>)evt.getSource();
	    	        if (evt.getClickCount() == 2) {

	    	            // Double-click detected
	    	            int index = list.locationToIndex(evt.getPoint());
	    	               if (index >= 0) {
	    	                  Object item = list.getModel().getElementAt(index);
	    	                  String usuario = item.toString();
	    	                  String[] parts = usuario.split(" ");
	    	                  String nom = parts[0];
	    	                  String ape = parts[1];
	    	           	    try{
	    	           	    	
	    	       	    	 ps=con.prepareStatement("select * from docente where nombre = '" + nom + "' and apellido = '" + ape + "'");
	    	       	    	 ps2=con.prepareStatement("select * from estudiante where nombre = '" + nom + "' and apellido = '" + ape + "'");
	    	                 rs=ps.executeQuery();
	    	                 rs2 = ps2.executeQuery();
	    	                    if(rs2.next()) {
	    	                    	 String nick = (rs2.getNString(1));
	    	                    	 String nombre = (rs2.getString(2)); 
	    	                         String apellido = (rs2.getString(3));
	    	                         String correo = (rs2.getNString(4));
	    	                         String fecha = (rs2.getString(5));
	    	                         String completo = "Nickname: " + nick + "\n" + "Nombre: " +nombre + "\n" + "Apellido: " + apellido + "\n" + "Correo: " +correo + "\nFecha: " + fecha;
		    	                     JOptionPane.showMessageDialog(null, completo, "Datos de Estudiante", JOptionPane.INFORMATION_MESSAGE);
	    	                    }
	    	                    if(rs.next()) {
	    	                    	String instituto = (rs.getString(5));
	    	                    	 String nick = (rs.getString(1));
	    	                    	 String nombre = (rs.getString(2)); 
	    	                         String apellido = (rs.getString(3));
	    	                         String correo = (rs.getNString(4));
	    	                         String fecha = (rs.getString(6));
	    	                         String completo = "Instituto: " + instituto + "\nNickname: " + nick + "\n" + "Nombre: " +nombre + "\n" + "Apellido: " + apellido + "\n" + "Correo: " +correo + "\nFecha: " + fecha;
		    	                     JOptionPane.showMessageDialog(null, completo, "Datos de Docente", JOptionPane.INFORMATION_MESSAGE);
	    	                    }
	    	               }
	    	               catch(Exception e)
	    	               {
	    	                   System.out.println("Error in getData"+e);
	    	               }
	    	                   
	    	                }
	    	        } 
	    	 }
	     });
	     list.setBorder(new LineBorder(new Color(0, 0, 0)));
	     list.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
	     scrollPane.setViewportView(list);

	     
	}

}
