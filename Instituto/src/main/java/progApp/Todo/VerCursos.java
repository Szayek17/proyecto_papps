package progApp.Todo;

//import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
//import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;


public class VerCursos extends JFrame {

	private JPanel contentPane;
	private static final long serialVersionUID = 1L;
    Connection con=null;
    PreparedStatement ps=null;
    PreparedStatement ps2=null;
    ResultSet rs=null;
    //ResultSet rs2=null;
    ArrayList<String> myList = new ArrayList<>();
    @SuppressWarnings("rawtypes")
	JList<?> list = new JList();


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VerCursos frame = new VerCursos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void DbConnection(){
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto","root","agusabE123@#");
			
		}catch(Exception e){ System.out.println(e);}  
	}

	/**
	 * Create the frame.
	 */
	public VerCursos() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		@SuppressWarnings("rawtypes")
		JList<?> list = new JList();
		
		JButton btnNewButton = new JButton("Salir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
			}
			
		});
		btnNewButton.setBounds(0, 238, 434, 23);
		contentPane.add(btnNewButton);
		DbConnection();
		try {
			
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from curso");
			while(rs.next()) {
				String prueba = (rs.getString(1));
				myList.add(prueba);
			}
			
		}
		catch(Exception e)
        {
            System.out.println("Error in getData"+e);
        }
		
//		list = new JList<Object>(myList.toArray());
//	     list.addMouseListener(new MouseAdapter() {
//	    	 public void mouseClicked(MouseEvent evt) {
//	    		 JList<?> list = (JList<?>)evt.getSource();
//	    	        if (evt.getClickCount() == 2) {
//
//	    	            // Double-click detected
//	    	            int index = list.locationToIndex(evt.getPoint());
//	    	               if (index >= 0) {
//	    	                  Object item = list.getModel().getElementAt(index);
//	    	                  String usuario = item.toString();
//	    	                  String[] parts = usuario.split(" ");
//	    	                  //String nom = parts[0];
//	    	                  //String ape = parts[1];
//	    	           	    try{
//	    	           	    	
//	    	       	    	 ps=con.prepareStatement("select * from curso");
//	    	                 rs=ps.executeQuery();
//	    	                    if(rs.next()) {
//	    	                    	 String nombre = (rs.getString(1)); 
//	    	                         Integer duracion = (rs.getInt(2));
//	    	                         Integer horas = (rs.getInt(3));
//	    	                         Integer creditos = (rs.getInt(4));
//	    	                         String duracion1 = String.valueOf(duracion);
//	    	                         String horas1 = String.valueOf(horas);
//	    	                         String creditos1 = String.valueOf(creditos);
//	    	                         String completo = "Cursos: " + "\nNombre: " + nombre + "\n" + "Duracion: " +duracion1 + "\n" + "Horas: " + horas1 + "\n" + "Creditos: " +creditos1;
//		    	                     JOptionPane.showMessageDialog(null, completo, "Datos de Docente", JOptionPane.INFORMATION_MESSAGE);
//	    	                    }
//	    	               }
//	    	               catch(Exception e)
//	    	               {
//	    	                   System.out.println("Error in getData"+e);
//	    	               }
//	    	                   
//	    	                }
//	    	        } 
//	    	 }
//	     });
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 434, 239);
		contentPane.add(scrollPane);
		
		list = new JList<Object>(myList.toArray());
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    list.setBorder(new LineBorder(new Color(0, 0, 0)));
	    list.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		scrollPane.setViewportView(list);
	}

}
