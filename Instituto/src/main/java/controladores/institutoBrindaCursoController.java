package controladores;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelos.manager.Manager;

import progApp.Todo.*;

public class institutoBrindaCursoController {
	private static Manager manag;
	private static EntityManager manager;
	private static EntityManagerFactory emf;
	

	public institutoBrindaCursoController() {
		manag = Manager.getInstance();
	}

	//tengo que sacar los datos de institutobrindacurso. tengo que conseguir los cursos que brinda el instituto
	
	public ArrayList<InstitutoBrindaCurso> obtenerCursos(String instituto){
		ArrayList<InstitutoBrindaCurso> cursos = (ArrayList<InstitutoBrindaCurso>)manag.getSessionManager().createQuery("from InstitutoBrindaCurso where nombreInst =?1").setParameter(1, instituto).getResultList();
			
		manag.closeSession();
	return cursos;
	}
	
	//Obtener el instituto de un curso.
	public String obtenerInstituto(String nomCurso) {
		String ins = (String) manag.getSessionManager().createQuery("SELECT nombreInst FROM InstitutoBrindaCurso WHERE nombreCurso=:name").setParameter("name", nomCurso).getSingleResult();
		manag.closeSession();
		return ins;
	}
} 