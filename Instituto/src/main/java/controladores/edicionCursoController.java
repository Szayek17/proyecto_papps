package controladores;

import java.time.LocalDate;
import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import progApp.Todo.edicionCurso;
import modelos.manager.Manager;

public class edicionCursoController {
	private static Manager manag;
	private static EntityManager manager;
	private static EntityManagerFactory emf;
	
	
	public edicionCursoController() {
		manag = Manager.getInstance();
	}
// le paso el curso y me tiene que devolver las ediciones
	public ArrayList<edicionCurso> listarEdCursos(String cur) {
		ArrayList<edicionCurso> edCursos = (ArrayList<edicionCurso>) manag.getSessionManager().createQuery("from edicionCurso where nomCurso =?1").setParameter(1, cur).getResultList();
		manag.closeSession();
		return (ArrayList<edicionCurso>) edCursos;
	}
	//aca le estoy pasando Dalavuelta 2020
	public ArrayList<edicionCurso> listarDatosEdCursos(String edCur) {
		ArrayList<edicionCurso> datosEd = (ArrayList<edicionCurso>) manag.getSessionManager().createQuery("from edicionCurso where nomEdicion =?1").setParameter(1, edCur).getResultList();
		
		manag.closeSession();
		return (ArrayList<edicionCurso>) datosEd;
	}
	
	
	public boolean edicionesVigentes(LocalDate fechaActual,LocalDate fechaInicio,LocalDate fechaFin) {
		
		boolean pertenece = false;
		
		LocalDate timestampF = fechaActual;
		//timestamppF es el de cada row
		if ((timestampF.equals(fechaInicio)) || (timestampF.equals(fechaFin))){
			//o es el mismo dia de los limites
			pertenece = true;
		}else if ((timestampF.isAfter(fechaInicio)) && (timestampF.isBefore(fechaFin))){
			pertenece = true;
			return pertenece;
		}
		return pertenece;		
	}
	
	//Elegir una edición de curso
	public edicionCurso elegirEdicion(String nombreEdicion) {
		edicionCurso edicion = (edicionCurso) manag.getSessionManager().createQuery("FROM edicionCurso WHERE nomEdicion=:name").setParameter("name", nombreEdicion).getSingleResult();
		manag.closeSession();
		return edicion;
	}
}/* fin clase controller*/
