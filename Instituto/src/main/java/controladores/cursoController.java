package controladores;


import java.util.ArrayList;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import progApp.Todo.Curso;
import modelos.manager.Manager;



public class cursoController {

	private static Manager manag;
	private static EntityManager man;
	private static EntityManagerFactory emf;
	
	public cursoController() {
		manag = Manager.getInstance();
	}
	
				 
	
	public static ArrayList<Curso> listarCursoss(String in) {
		
		List<Curso> cursos = (List<Curso>) manag.getSessionManager().createQuery("from institutobrindacurso where nombreInst =:in").setParameter("in", in).getResultList();
		manag.closeSession();
		return (ArrayList<Curso>) cursos;
		
	}	
/*---------------------------------------------------------------------------------------------------------------------------------------*/
	public ArrayList<Curso> listarCursos(String inst) {
		
		
		//opcion1
		ArrayList<Curso> cursos = (ArrayList<Curso>) manag.getSessionManager().createQuery("from InstitutoBrindaCurso where nombreInst =?1").setParameter(1, inst).getResultList();
		//opcion2
		// ArrayList<Curso> cursos = (ArrayList<Curso>) manag.getSessionManager().createQuery("from Curso where nombreInst =:in").setParameter("in",in).getResultList();
		//opcion3
		//String hql = "from Curso where nombreInst =:in";
		//Query query = manag.getSessionManager().createQuery(hql);
		//query.setParameter("in", in);
		//List results = query.list();
			
		manag.closeSession();
		
		return (ArrayList<Curso>) cursos;
	}
	
	//Hice un tercer método para listar cursos porque los que hay me dan error con el cast pero no los quise tocar por las dudas.
	@SuppressWarnings("unchecked")
	public List<String> listarNombreCursos(String nomIns) {
		List<String> cursos = new ArrayList<String>();
		cursos = (List<String>) manag.getSessionManager().createQuery("SELECT nombreCurso FROM InstitutoBrindaCurso WHERE nombreInst=:name").setParameter("name", nomIns).getResultList();
		manag.closeSession();
		return cursos;
	}
	
	//Obtener un curso por su nombre
	public Curso elegirCurso(String nom) {
		Curso curso = (Curso) manag.getSessionManager().createQuery("FROM Curso WHERE nombre = :name").setParameter("name", nom).getSingleResult();
		manag.closeSession();
		return curso;
	}
	
	//Obtener las previas de un curso
	@SuppressWarnings("unchecked")
	public List<String> listarPrevias(String nomCurso) {
		List<String> lista = (List<String>) manag.getSessionManager().createQuery("SELECT nomPrevia FROM cursoPrevias WHERE nomCurso=:name").setParameter("name", nomCurso).getResultList();
		manag.closeSession();
		return lista;
	}
}

