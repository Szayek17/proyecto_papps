package controladores;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CursosIntProgramasController {
	//Fábrica para obtener un manager.
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Institutos");
	
	//Métodos
	@SuppressWarnings("unchecked")//Para evitar warning por el cast.
	public static List<String> listarProgramasDeCurso(String nomCurso) {
		EntityManager manager = emf.createEntityManager();
		List<String> listaNomProg = (List<String>) manager.createQuery("SELECT nombreProg FROM CursosIntProgramas WHERE nombreCurso=:name").setParameter("name", nomCurso).getResultList();
		manager.close();
		return listaNomProg;
	}
}
