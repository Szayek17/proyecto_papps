package controladores;

import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import modelos.InscripcionEdCurso;
import modelos.manager.Manager;

public class inscripcionEdCursoController {
	private static Manager manag;
	private static EntityManager manager;
	private static EntityManagerFactory emf;



	public inscripcionEdCursoController() {
		manag = Manager.getInstance();
	}

	public boolean altaInscripEdCurso(String nombreEdCurso, String nombreEst, LocalDate fecha) {
		boolean existe = hayInscripcionRepetida(nombreEst, nombreEdCurso, fecha);
			if(existe == false) {
		manag.startTransaction("InscripcionEdCurso", new InscripcionEdCurso(nombreEdCurso,nombreEst,fecha));
			}
		return existe;
	}
	
	boolean hayInscripcionRepetida( String nombreEst, String nombreEdCurso, LocalDate fecha) {
		boolean existe = true;
		try {
			
			InscripcionEdCurso insCurso = (InscripcionEdCurso) manag.getSessionManager().createQuery("From InscripcionEdCurso where nomEstudiante =?1 AND edicion =?2 AND fechaInscripcion =?3").setParameter(1, nombreEst).setParameter(2, nombreEdCurso).setParameter(3, fecha).getSingleResult();
			manag.closeSession();
			
		} catch(Exception e) {
			//System.out.println(e.toString());
			return false;
			
		}
		return existe;
		
	}
	//En la base de datos tiene que tener la PK Compuesta para funcionar bien, con las relaciones.
}//Fin Clase


