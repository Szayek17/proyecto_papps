package controladores;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import progApp.Todo.Prog_Formacion;

public class Prog_FormacionController {
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Institutos");
	
	public Prog_FormacionController() {
	
	}
	
	public static Prog_Formacion elegirPrograma(String nombre) {
		EntityManager manager = emf.createEntityManager();
		
		Prog_Formacion programa = (Prog_Formacion) manager.createQuery("FROM Prog_Formacion WHERE nombreForm=:name").setParameter("name", nombre).getSingleResult();
		
		manager.close();
		
		return programa;
	}
}
