package controladores;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;


import progApp.Todo.Instituto;
import modelos.manager.Manager;


public class institutoController {
	
	private static Manager manag;
	private static EntityManager man;
	private static EntityManagerFactory emf;

	public institutoController() {
		
		manag = Manager.getInstance();
	}

	public static void main(String[] args) {
		manag = Manager.getInstance();
		//emf = Persistence.createEntityManagerFactory("Persistencia");
		//man = emf.createEntityManager();
		/*
		Instituto i1 = new Instituto("Cure","Gualby","EDA");
		Instituto i2 = new Instituto("Rocha","Sasi","ADI");
		
		man.getTransaction().begin();
		man.persist(i1);
		man.persist(i2);
		man.getTransaction().commit();*/
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Instituto> listarInstitutos() {
		
		List<Instituto> institutos = (List<Instituto>) manag.getSessionManager().createQuery("FROM Instituto").getResultList();
		
		manag.closeSession();
		return (ArrayList<Instituto>)institutos;
		
	}
	
}//Fin clase


