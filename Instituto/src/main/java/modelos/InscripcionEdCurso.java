package modelos;
import javax.persistence.Entity;
import javax.persistence.Table;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Id;

@Entity
@Table(name = "inscripcionedcurso")
public class InscripcionEdCurso {
	
	
	
	@Column(name = "edicion")
	private String edicion;
	@Id
	@Column(name = "nomEstudiante")
	private String nomEstudiante;
	
	@Column(name = "fechaInscripcion")
	private LocalDate fechaInscripcion;

	public InscripcionEdCurso() {
		
	}
	
	public InscripcionEdCurso(String edicion, String nomEstudiante, LocalDate fechaInscripcion) {
		//super();
		this.edicion = edicion;
		this.nomEstudiante = nomEstudiante;
		this.fechaInscripcion = fechaInscripcion;
	}

	
	public String getEdicion() {
		return edicion;
	}

	public void setEdicion(String edicion) {
		this.edicion = edicion;
	}

	public String getNomEstudiante() {
		return nomEstudiante;
	}

	public void setNomEstudiante(String nomEstudiante) {
		this.nomEstudiante = nomEstudiante;
	}

	public LocalDate getFechaInscripcion() {
		return fechaInscripcion;
	}

	public void setFechaInscripcion(LocalDate fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}

}

