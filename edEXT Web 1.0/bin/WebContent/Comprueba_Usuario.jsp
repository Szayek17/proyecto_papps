<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%@ page import="java.sql.*" %>

<%

	String nickname = request.getParameter("nick");
	String correo = request.getParameter("correo");
	Class.forName("com.mysql.jdbc.Driver");
	
	try {
		Connection miConexion = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
		
		PreparedStatement c_preparada = miConexion.prepareStatement("SELECT * FROM USUARIO WHERE NICKNAME=? AND CORREO=?");
		
		PreparedStatement c_preparadaProfe = miConexion.prepareStatement("SELECT * FROM DOCENTE WHERE NICKNAME=? AND CORREO=?");
		PreparedStatement c_preparadaEstudiante = miConexion.prepareStatement("SELECT * FROM ESTUDIANTE WHERE NICKNAME=? AND CORREO=?");

		c_preparadaProfe.setString(1, nickname);
		c_preparadaProfe.setString(2, correo);
		c_preparadaEstudiante.setString(1, nickname);
		c_preparadaEstudiante.setString(2, correo);
		ResultSet resultadoObtenido = c_preparadaProfe.executeQuery();
		ResultSet resultadoObtenido2 = c_preparadaEstudiante.executeQuery();

		
		if(resultadoObtenido.next()) {
		    String site = new String("http://localhost:8080/Primeros_Pasos_JSP/loginValido.html");
		    response.setStatus(response.SC_MOVED_TEMPORARILY);
		    response.setHeader("Location", site);
		} else if(resultadoObtenido2.next()) {
		    String site = new String("http://localhost:8080/Primeros_Pasos_JSP/loginValidoEst.html");
		    response.setStatus(response.SC_MOVED_TEMPORARILY);
		    response.setHeader("Location", site);
		} else {
		    String site = new String("http://localhost:8080/Primeros_Pasos_JSP/loginInvalido.html");
		    response.setStatus(response.SC_MOVED_TEMPORARILY);
		    response.setHeader("Location", site);
		}
		
	} catch(Exception e) {
	}

%>
</body>
</html>