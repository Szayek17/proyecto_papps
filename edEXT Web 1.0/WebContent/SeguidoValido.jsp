<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>edEXT</title>

<link rel="stylesheet" type="text/css" href="1-simple-calendar/tcal.css" />
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<script type="text/javascript" src="1-simple-calendar/tcal.js"></script>

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilos.css">


</head>
<body class=bg-dark>
	<%
	String nick = session.getAttribute("MiNick").toString();
	String nickSeguido = request.getParameter("selectUsu");

	Class.forName("com.mysql.jdbc.Driver");
	java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
	Statement statement = null;
	int resultCount;
try{
	statement=connection.createStatement();
	String sql ="INSERT INTO usuario_sigue (nickname, nickname_seguido) VALUES ('" + nick +"', '" + nickSeguido +"');";
	resultCount = statement.executeUpdate(sql);
	if(resultCount<0)
		throw new Exception("No hay informaci�n disponible sobre el recuento de actualizaciones al ejecutar la sentencia.");
} catch (Exception e) {
	e.printStackTrace();
}
%>
	<div>
		<h1 class="display-4 text-center pt-2">Usuario seguido
			correctamente!</h1>
		<hr class="bg-warning">
		<br> <br>
		<div class="w3-container w3-center w3-animate-zoom">
			<div class="row justify-content-center align-items-center"
				style="height: 50px;">
				<div class="col-0">
					<img src="img/usuarioSeguido.png" width="83" height="85"> <br>
				</div>
			</div>
			<div class="row justify-content-center align-items-center"
				style="height: 140px;">
				<div class="col-0">
					<h3>
						Ha comenzado a seguir al usuario:
						<%=nickSeguido %></h3>
					<!-- No se incluye el bot�n aceptar ya que al redigir al mismo index, es obsoleta. -->
				</div>
			</div>
		</div>
	</div>
</body>
</html>