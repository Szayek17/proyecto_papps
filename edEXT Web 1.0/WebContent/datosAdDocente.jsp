<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>edEXT:: Datos Adicionales</title>

	<link rel="stylesheet" type="text/css" href="1-simple-calendar/tcal.css" />
	<script type="text/javascript" src="1-simple-calendar/tcal.js"></script>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/estilos.css">

</head>

<body class="bg-dark">
<h1 class="display-4 text-center pt-3">Datos Adicionales</h1>
<hr class="bg-warning">
<h4 class="display-5 text-center pt-3">Estudiantes Inscriptos a Mis Ediciones</h4> <br>
<%
Statement statement = null;
ResultSet resultSet = null;

String nick = session.getAttribute("MiNick").toString();


Class.forName("com.mysql.jdbc.Driver");
java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");

try{
	statement=connection.createStatement();
	String sql ="select * from docentedaedcurso natural join inscripcionedcurso where refDocente = '" + nick  +"' AND refEdCurso = edicion";
	resultSet = statement.executeQuery(sql);
	%>
<select name = "test" class="form-control" style="width:20%; margin:auto; text_align:center" >
<%
	while(resultSet.next())
	{
		String fname = resultSet.getString("nomEstudiante") + " - " + resultSet.getString("edicion"); 
%>
<option value="<%=fname %>"><%=fname %></option>
<%
	}
%>
</select>
<br>
	<%
	} catch (Exception e) {
	e.printStackTrace();
	}

%>
</body>
</html>