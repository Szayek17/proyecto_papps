<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>edEXT::</title>

<link rel="stylesheet" type="text/css" href="1-simple-calendar/tcal.css" />
<script type="text/javascript" src="1-simple-calendar/tcal.js"></script>

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilos.css">
<style type="text/css">
div.avatar {
    height: 180px;
    width: 180px;
    background-repeat: no-repeat;
    background-position: 50%;
    border-radius: 50%;
}
</style>
</head>

<body class="bg-dark">
	<h1 class="display-4 text-center pt-3">Datos de la Edicion</h1>
	<hr class="bg-warning">
	<%
Statement statement = null;
ResultSet resultSet = null;
String nomE = request.getParameter("selectEdicion");
try{
java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
statement=connection.createStatement();
String sql ="select * from edicioncurso where nomEdicion='" + nomE +"';";
resultSet = statement.executeQuery(sql);
int cantHoras;
int cantCreditos;
while(resultSet.next()){
%>
	<div style="padding: 0px 100px 50px 150px;">
		<div class="row justify-content-center" style="margin-bottom: 25px;">
			<div class="col-md-3 align-self-start">
				<%
				String img=resultSet.getString("img");
				if(img==null){%>
					<div class="avatar" style="background-image: url(img/image-not-found.png); background-size: 76% auto;"></div>
				<%
				} else {
				%>
					<div class="avatar" style="background-image: url(<%=resultSet.getString("img")%>); background-size: 100% auto;"></div>
				<%} %>
			</div>
			<div class="col-md-3 align-self-center">
				<h2 class="font-weight-bold" ><%=resultSet.getString("nomEdicion") %></h2>
			</div>
		</div>
		<div class="row form-group">
			<label class="col-form-label col-md-2" for="formGroupExampleInput">fecha de inicio:</label>
			<div class="col-md-9">
				<input type="text" class="form-control" name="fechaIni"
					id="fechaIni"
					value="<%=resultSet.getString("fechaIni") %>" readonly>
			</div>
		</div>
		<div class="row form-group">
			<label class="col-form-label col-md-2" for="formGroupExampleInput">fecha de finalizacion:</label>
			<div class="col-md-9">
				<input type="text" class="form-control" name="fechaFin"
					id="fechaFin" value="<%=resultSet.getString("fechaFin") %>"
					readonly>
			</div>
		</div>
		<div class="row form-group">
			<label class="col-form-label col-md-2" for="formGroupExampleInput">fecha de publicacion:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="fechaPublicado"
					id="fechaPublicado" value="<%=resultSet.getString("fechaPublicado") %>"
					readonly>
			</div>
		</div>
		<div class="row form-group">
			<label class="col-form-label col-md-2" for="formGroupExampleInput">cupos:</label>
			<div class="col-sm-9">
				<%
				if (resultSet.getString("cupos") == "-1") {
				%>
				<input type="text" class="form-control" name="cupos"
					id="cupos"
					value="Sin cupos disponibles." readonly>
				<%
				} else {
				%>
				<input type="text" class="form-control" name="cupos"
					id="cupos"
					value="<%=resultSet.getString("cupos") %>" readonly>
				<%} %>
			</div>
		</div>
		<div class="row form-group">
			<label class="col-form-label col-md-2" for="formGroupExampleInput">Curso asociado:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="nomCurso" id="nomCurso"
					value="<%=resultSet.getString("nomCurso") %>" readonly>
			</div>
		</div>
		<div class="row justify-content-center align-items-center"
			style="height: 60px;">
			<div class="col-0">
				<label >Docentes:</label>
			</div>
		</div>
		<div class="row justify-content-center align-items-center"
			style="height: 170px;">
			<div class="col-6">
				<%
				statement = null;
				resultSet = null;
				try{
					statement=connection.createStatement();
					sql ="select * from docente where nickname in (select refDocente from docentedaedcurso where refEdCurso='" + nomE +"');";
					resultSet = statement.executeQuery(sql);
				%>
				<select id="selectDocente" name="selectDocente" class="form-control"
					style="width: 65%; margin: auto; text_align: center" size="10">

					<%
				if(!resultSet.isBeforeFirst()){
				%>
					<option value="" disabled>-Sin Docentes-</option>
					<%
				} else { 
				%>
					<option value="" disabled>Nick -- Nombre Apellido</option>
					<option value="" disabled>===================</option>
				<%
				}
					while(resultSet.next())
					{
						String nomDoc = resultSet.getString("nombre");
						String apellidoDoc = resultSet.getString("apellido");
						String nickDoc = resultSet.getString("nickname");
				%>
					<option value="<%=nickDoc %>" disabled><%=nickDoc + " -- " + nomDoc + " " + apellidoDoc %></option>
					<%
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				%>
				</select>
			</div>
		</div>
	</div>
	<%
}
connection.close();
} catch (Exception e) {
e.printStackTrace();
}
%>
</body>
</html>