<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">

<title>Insert title here</title>
</head>
<body>
<body class="bg-dark">

	<h1 class="display-4 text-center pt-3" style="color: white">Seleccione
		para ver los cursos registrados</h1>
	<hr class="bg-warning">
	<div class="container">
		<div class="row justify-content-around align-items-center"
			style="height: 200px;">
			<div class="col-4">
				<%

				Class.forName("com.mysql.jdbc.Driver");
				java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
				Statement statement = null;
				ResultSet resultSet = null;
				try{
					statement=connection.createStatement();
					String sql ="select * from instituto";
					resultSet = statement.executeQuery(sql);
				%>
				<select id="selectIns" name="selectIns" class="form-control"
					style="width: 67%; margin: auto; text_align: center" required>
					<option value="" selected disabled>Seleccione instituto</option>

					<%
					while(resultSet.next())
					{
						String fname = resultSet.getString("NInstituto"); 
				%>
					<option value="<%=fname %>"><%=fname %></option>
					<%
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				%>
				</select>
			</div>
			<div class="col-4">
				<%
				try{
					statement=connection.createStatement();
					String sql ="select * from categoria";
					resultSet = statement.executeQuery(sql);
				%>
				<select id="selectCat" name="selectCat" class="form-control"
					style="width: 70%; margin: auto; text_align: center" required>
					<option value="" selected disabled>Seleccione categoria</option>

					<%
					while(resultSet.next())
					{
						String fname = resultSet.getString("nomCategoria"); 
				%>
					<option value="<%=fname %>"><%=fname %></option>
					<%
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				%>
				</select>
			</div>
		</div>
		<div class="row justify-content-around align-items-center"
			style="height: 0px;">
			<div class="col-2">
				<a class="btn btn-warning float-left" id="ins" onclick="refIns()">Ver los
				cursos que brinda</a>
			</div>
			<div class="col-2">
				<a class="btn btn-warning float-right" id="cat" onclick="refCat()">Ver los
				cursos que contiene</a>
			</div>
		</div>
	</div>


	<script type="text/javascript">
		function refIns() {
			var nom = document.getElementById("selectIns").value;
			var dir = "http://localhost:8080/Proyecto_full/selectCurso_EdicionCurso.jsp";
			document.getElementById("ins").setAttribute("href",
					dir.concat("?select=", nom, "&verPor=ins"));
		};
		function refCat() {
			var nom = document.getElementById("selectCat").value;
			var dir = "http://localhost:8080/Proyecto_full/selectCurso_EdicionCurso.jsp";
			document.getElementById("cat").setAttribute("href",
					dir.concat("?select=", nom, "&verPor=cat"));
		};
	</script>
</body>
</html>
