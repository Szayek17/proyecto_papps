<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">

<title>edEXT</title>
</head>
<body class="bg-dark">
	<form action="selectUsuario.jsp" method="post">
	<h1 class="display-4 text-center pt-3" style="color: white">Seleccion de Estudiantes</h1>
	<hr class="bg-warning">
		<div class="row justify-content-around align-items-center"
			style="height: 200px;">
			<div class="col-4">
				<%

				Class.forName("com.mysql.jdbc.Driver");
				java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
				Statement statement = null;
				ResultSet resultSet = null;
				String nickname = session.getAttribute("NickIniciado").toString(); 
				try{
					statement=connection.createStatement();
					String sql ="select nomEdicion, refDocente from edicioncurso natural join docentedaedcurso where refDocente = '" + nickname +"' and nomEdicion = refEdCurso;";
					resultSet = statement.executeQuery(sql);
				%>
				<select id="selectEdi" name="selectEdi" class="form-control"
					style="width: 67%; margin: auto; text_align: center" required>
					<option value="" selected disabled>Seleccione Ed. Curso</option>

					<%
					while(resultSet.next())
					{
						String fname = resultSet.getString("nomEdicion"); 
				%>
					<option value="<%=fname %>"><%=fname %></option>
					<%
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				%>
				</select>
				<br>
				<table align="center">
					    <tr>
      			<td colspan="2" align="center">                    
      			<button type="submit" onclick="validar()" class="btn btn-warning"><i
                            class="fas fa-sign-in-alt"></i><a id="eti" target="_parent"> Elegir Usuarios</a>
          	</button>
    		</tr>
				</table>
				
				
			</div>
				
		</div>
		
	</div>

</form>
	<script type="text/javascript">
		function refIns() {
			var nom = document.getElementById("selectIns").value;
			var dir = "http://localhost:8080/Proyecto_full/inscripcionCurso_EdicionCurso.jsp";
			document.getElementById("eti").setAttribute("href",
					dir.concat("?select=", nom, "&verPor=ins"));
		};
	</script>
			
</body>
</html>
			