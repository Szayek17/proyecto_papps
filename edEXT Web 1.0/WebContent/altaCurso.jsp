<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>edEXT:: Alta Curso</title>

	<link rel="stylesheet" type="text/css" href="1-simple-calendar/tcal.css" />
	<script type="text/javascript" src="1-simple-calendar/tcal.js"></script>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/estilos.css">

</head>

<body class="bg-dark">
<iframe name="hiddenFrame" width="0" height="0" border="0" style="display: none;"></iframe>	<div class="container">
		<div>
			<h1 class="display-4 text-center pt-3">Registro de Curso</h1>
			<hr class="bg-warning">
			<form action="cursoAgregado.jsp" method="post">
<%

	Class.forName("com.mysql.jdbc.Driver");
	java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
	Statement statement = null;
	ResultSet resultSet = null;
	try{
		statement=connection.createStatement();
		String sql ="select * from instituto";
		resultSet = statement.executeQuery(sql);
		%>
	<select name = "test1" class="form-control" style="width:20%; margin:auto; text_align:center" >
		<option value="" selected disabled>Seleccione Instituto</option>
	
	<%
		while(resultSet.next())
		{
			String fname = resultSet.getString("Ninstituto"); 
	%>
	<option value="<%=fname %>"><%=fname %></option>
	<%
		}
	%>
	</select>
	<br>
		<%
		} catch (Exception e) {
		e.printStackTrace();
		}

%>
	<div>
	<h1 class="display-8 text-center pt-3" style="font-size:20px">Para seleccionar varias categorias, haga click mientras presiona ctrl.</h1>
	<br>
<%

	Class.forName("com.mysql.jdbc.Driver");
	java.sql.Connection connection3 = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
	Statement statement3 = null;
	ResultSet resultSet3 = null;

try{
statement3=connection3.createStatement();
String sql ="select * from categoria";
resultSet3 = statement3.executeQuery(sql);
%>
<select name = "categoria" class="form-control" style="width:20%; margin:auto; text_align:center" multiple>	
<%
while(resultSet3.next())
{
String fname = resultSet3.getString("nomCategoria"); 
%>
<option value="<%=fname %>"><%=fname %></option>
<%
}
%>
</select>
<br>

<%
} catch (Exception e) {
e.printStackTrace();
}

%>
	</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Nombre:</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="nombre" id="nombre">
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Descripcion:</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="descripcion" id="descripcion">
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Duracion (Semanas):</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="duracion" id="duracion">
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Cantidad de horas:</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="horas" id="horas">
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Cantidad de creditos:</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="creditos" id="creditos">
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">URL:</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="url" id="url">
					</div>
				</div>
				<div>
				<h1 class="display-8 text-center pt-3" style="font-size:20px">Para seleccionar varias previas, haga click mientras presiona ctrl.</h1>
				<br>
			
			<%

	Statement statement2 = null;
	ResultSet resultSet2 = null;
	java.sql.Connection connection2 = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");

	try{
		statement2=connection2.createStatement();
		String sql ="select * from curso";
		resultSet2 = statement2.executeQuery(sql);
		%>
	<select name = "test" class="form-control" style="width:20%; margin:auto; text_align:center" multiple>	
	<%
		while(resultSet2.next())
		{
			String fname = resultSet2.getString("nomCurso"); 
	%>
	<option value="<%=fname %>"><%=fname %></option>
	<%
		}
	%>
	</select>
	<br>

		<%
		} catch (Exception e) {
		e.printStackTrace();
		}

%>
		</div>		
		<br>
				<table align="center">
					    <tr>
      			<td colspan="2" align="center">                    
      			<button type="submit" onclick="validar()" class="btn btn-warning"><i
                            class="fas fa-sign-in-alt"></i><a id="eti" target="_parent"> Registrar</a>
          	</button>
          	
    		</tr>
				</table>
			</form>
		</div>
	</div>
</body>
<script>
	function habilitar() {
		document.getElementById("instituto").disabled = false;
		var radList = document.getElementsByName("customRadio");

		for (var i = 0; i < customRadio.length; i++) {
			if (customRadio[i].checked) document.getElementById(customRadio[i].id).checked = false;
		}
		//document.getElementById("customRadioAlumno").value = false;
	}
	function deshabilitar() {
		document.getElementById("instituto").disabled = true;
		//document.getElementById("customRadioDocente").value = true;
	}
</script>

</html>