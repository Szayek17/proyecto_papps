<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>edEXT::</title>

<link rel="stylesheet" type="text/css" href="1-simple-calendar/tcal.css" />
<script type="text/javascript" src="1-simple-calendar/tcal.js"></script>

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilos.css">
<style type="text/css">
div.avatar {
    height: 180px;
    width: 180px;
    background-repeat: no-repeat;
    background-position: 50%;
    border-radius: 50%;
}
</style>
</head>

<body class="bg-dark">
	<h1 class="display-4 text-center pt-3">Datos del Curso</h1>
	<hr class="bg-warning">
	<%
Statement statement = null;
ResultSet resultSet = null;
String nomC = request.getParameter("selectCurso");
try{
java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
statement=connection.createStatement();
String sql ="select * from curso where nomCurso='" + nomC +"';";
resultSet = statement.executeQuery(sql);
int cantHoras;
int cantCreditos;
while(resultSet.next()){
%>
	<div style="padding: 0px 100px 50px 150px;">
		<div class="row justify-content-center" style="margin-bottom: 25px;">
			<div class="col-md-3 align-self-start">
				<%
				String img=resultSet.getString("img");
				System.out.println(img);
				if(img==null){%>
					<div class="avatar" style="background-image: url(img/image-not-found.png); background-size: 76% auto;"></div>
				<%
				} else {
				%>
					<div class="avatar" style="background-image: url(<%=resultSet.getString("img")%>); background-size: 100% auto;"></div>
				<%} %>
			</div>
			<div class="col-md-3 align-self-center">
				<h2 class="font-weight-bold" ><%=resultSet.getString("nomCurso") %></h2>
			</div>
		</div>
		<div class="row form-group">
			<label class="col-form-label col-md-2" for="formGroupExampleInput">Descripcion:</label>
			<div class="col-md-9">
				<p class="p-2 text-dark"
					style="border-radius: 5px 5px 5px 5px; background-color: #eae9f2;"><%=resultSet.getString("descripcion") %></p>
			</div>
		</div>
		<div class="row form-group">
			<label class="col-form-label col-md-2" for="formGroupExampleInput">Duracion:</label>
			<div class="col-md-9">
				<input type="text" class="form-control" name="duracion"
					id="duracion"
					value="<%=resultSet.getString("duracion") %> semana/s" readonly>
			</div>
		</div>
		<div class="row form-group">
			<label class="col-form-label col-md-2" for="formGroupExampleInput">Cantidad
				de Horas:</label>
			<div class="col-md-9">
				<input type="text" class="form-control" name="cantHoras"
					id="cantHoras" value="<%=resultSet.getString("cantHoras") %>"
					readonly>
			</div>
		</div>
		<div class="row form-group">
			<label class="col-form-label col-md-2" for="formGroupExampleInput">Cantidad
				de Creditos:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="cantCreditos"
					id="cantCreditos" value="<%=resultSet.getString("cantCreditos") %>"
					readonly>
			</div>
		</div>
		<div class="row form-group">
			<label class="col-form-label col-md-2" for="formGroupExampleInput">Fecha
				de publicacion:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="fechaPublicado"
					id="fechaPublicado"
					value="<%=resultSet.getString("fechaPublicado") %>" readonly>
			</div>
		</div>
		<div class="row form-group">
			<label class="col-form-label col-md-2" for="formGroupExampleInput">URL:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="url" id="url"
					value="<%=resultSet.getString("url") %>" readonly>
			</div>
		</div>
		<div class="row justify-content-around align-items-center"
			style="height: 100px;">
			<label class="col-form-label col-md-2">Programas de Formacion</label> <label
				class="col-form-label col-md-2">Ediciones del Curso</label>
		</div>
		<div class="row justify-content-around align-items-center"
			style="height: 50px;">
			<div class="col-5">
				<%
				statement = null;
				resultSet = null;
				try{
					statement=connection.createStatement();
					sql ="select * from cursosintprogramas where nombreCurso='" + nomC +"';";
					resultSet = statement.executeQuery(sql);
				%>
				<select id="selectProg" name="selectProg" class="form-control"
					style="width: 67%; margin: auto; text_align: center" required>

					<%
				if(resultSet.isBeforeFirst()){
				%>
					<option value="" selected disabled>Seleccionar Programa</option>
					<%
				} else {
				%>
					<option value="" selected disabled>-Sin Programas-</option>
					<%
				}
					while(resultSet.next())
					{
						String fname = resultSet.getString("nombreProg"); 
				%>
					<option value="<%=fname %>"><%=fname %></option>
					<%
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				%>
				</select>
			</div>
			<div class="col-5">
				<%
				try{
					statement=connection.createStatement();
					sql ="select * from edicioncurso where nomCurso='" + nomC +"';";
					resultSet = statement.executeQuery(sql);
				%>
				<select id="selectEdicion" name="selectEdicion" class="form-control"
					style="width: 70%; margin: auto; text_align: center" required>
					<%
				if(resultSet.isBeforeFirst()){
				%>
					<option value="" selected disabled>Seleccionar Edicion</option>
					<%
				} else {
				%>
					<option value="" selected disabled>-Sin Ediciones-</option>
					<%
				}
					while(resultSet.next())
					{
						String fname = resultSet.getString("nomEdicion"); 
				%>
					<option value="<%=fname %>"><%=fname %></option>
					<%
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				%>
				</select>
			</div>
		</div>
		<div class="row justify-content-around align-items-center"
			style="height: 100px;">
			<div class="col-3">
				<a class="btn btn-warning form-control" id="prog"
					onclick="refProg()">Ver datos</a>
			</div>
			<div class="col-3">
				<a class="btn btn-warning form-control" id="ed" onclick="refEd()">Ver
					datos</a>
			</div>
		</div>

	</div>

	<%
}
connection.close();
} catch (Exception e) {
e.printStackTrace();
}
%>

	<script type="text/javascript">
		function refProg() {
			var nom = document.getElementById("selectProg").value;
			var dir = "http://localhost:8080/Proyecto_full/infoProgForm.jsp";
			document.getElementById("prog").setAttribute("href",
					dir.concat("?test=", nom));
		};
		function refEd() {
			var nom = document.getElementById("selectEdicion").value;
			var dir = "http://localhost:8080/Proyecto_full/infoEdicionCurso.jsp";
			document.getElementById("ed").setAttribute("href",
					dir.concat("?selectEdicion=", nom));
		};
	</script>
</body>
</html>
