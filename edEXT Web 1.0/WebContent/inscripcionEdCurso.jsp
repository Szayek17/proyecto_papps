<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">

<title>edEXT</title>
</head>
<body class="bg-dark">

	<h1 class="display-4 text-center pt-3" style="color: white">Seleccione
		instituto</h1>
	<hr class="bg-warning">
		<div class="row justify-content-around align-items-center"
			style="height: 200px;">
			<div class="col-4">
				<%

				Class.forName("com.mysql.jdbc.Driver");
				java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
				Statement statement = null;
				ResultSet resultSet = null;
				try{
					statement=connection.createStatement();
					String sql ="select * from instituto";
					resultSet = statement.executeQuery(sql);
				%>
				<select id="selectIns" name="selectIns" class="form-control"
					style="width: 67%; margin: auto; text_align: center" required>
					<option value="" selected disabled>Seleccione instituto</option>

					<%
					while(resultSet.next())
					{
						String fname = resultSet.getString("NInstituto"); 
				%>
					<option value="<%=fname %>"><%=fname %></option>
					<%
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				%>
				</select>
				<br>
			<table align="center">
			<tr>
    		<td colspan="2" align="center">

		<a class="btn btn-warning" style="color:black" id="eti" onclick="refIns()">Ver los cursos que brinda</a>
    		</tr>
			</table>
				
				
			</div>
				
		</div>
		
	</div>


	<script type="text/javascript">
		function refIns() {
			var nom = document.getElementById("selectIns").value;
			var dir = "http://localhost:8080/Proyecto_full/inscripcionCurso_EdicionCurso.jsp";
			document.getElementById("eti").setAttribute("href",
					dir.concat("?select=", nom, "&verPor=ins"));
		};
	</script>
			
</body>
</html>
			