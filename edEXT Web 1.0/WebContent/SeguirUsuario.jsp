<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/bootstrap.min.css">

<title>Insert title here</title>
</head>
<body>
<body class="bg-dark">
	<div class="container">
		<div class="row justify-content-center"
			style="height: 90px;">
			<div class="col-0 align-self-end">
				<img src="img/usuarioSeguir.png" width="80" height="80">
			</div>
			<div class="col-0 align-self-center">
				<h1 class="display-4 text-center pt-3" style="color: white">Seguir
					un Usuario</h1>
			</div>
		</div>
	</div>
	<hr class="bg-warning">
	<form action="SeguidoValido.jsp" method="get">
		<div style="text-align: center; height: 50px;">
			<h3 class="text-light">Seleccionar:</h3>
		</div>
		<%
	String nickSesion = session.getAttribute("MiNick").toString();
	Class.forName("com.mysql.jdbc.Driver");
	java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
	Statement statementDoc = null;
	Statement statementEst = null;
	ResultSet resultSetDoc = null;
	ResultSet resultSetEst = null;
	String correo;
	String nick;
	try{
		statementDoc=connection.createStatement();
		statementEst=connection.createStatement();
		String sql;
		sql ="select * from docente where nickname not in (select nickname_seguido from usuario_sigue where nickname='" + nickSesion + "');";
		resultSetDoc = statementDoc.executeQuery(sql);
		sql ="select * from estudiante where nickname not in (select nickname_seguido from usuario_sigue where nickname='" + nickSesion + "');";
		resultSetEst = statementEst.executeQuery(sql);
		%>
		<select name="selectUsu" class="form-control"
			style="width: 35%; margin: auto; text_align: center" size="8" required>
			<option value="" disabled>-Docentes-</option>
			<%
	while(resultSetDoc.next()) {
		nick = resultSetDoc.getString("nickname");
		correo = resultSetDoc.getString("correo");
	%>
			<option value="<%=nick%>"><%=nick + " -- " + correo %></option>
			<%
	}
	%>
			<option value="" disabled></option>
			<option value="" disabled>-Estudiantes-</option>
			<%
	while(resultSetEst.next())
	{
		nick = resultSetEst.getString("nickname");
		correo = resultSetEst.getString("correo"); 
	%>
			<option value="<%=nick%>"><%=nick + " -- " + correo %></option>
			<%
	}
	%>
		</select> <br>
		<div style="text-align: center">
			<input type="submit" value="Seguir usuario" class="btn btn-warning">
		</div>
		<%
		} catch (Exception e) {
		e.printStackTrace();
		}

%>
	</form>
</body>
</html>