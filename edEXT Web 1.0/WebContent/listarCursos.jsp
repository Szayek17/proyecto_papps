<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="css/bootstrap.min.css">

<title>Insert title here</title>
</head>
<body>
<body class="bg-dark">

<h1 class="display-4 text-center pt-3" style="color:white">Cursos Registrados</h1>
<hr class="bg-warning">
<form action="altaCursoaPrograma.jsp" method="post">
<%

	String prog = request.getParameter("prog");
	session.setAttribute("prog",prog);
	Class.forName("com.mysql.jdbc.Driver");
	java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
	Statement statement = null;
	ResultSet resultSet = null;
	try{
		statement=connection.createStatement();
		String sql ="select * from curso";
		resultSet = statement.executeQuery(sql);
		%>
	<select name = "curso" class="form-control" style="width:20%; margin:auto; text_align:center" >
		<option value="" selected disabled>Seleccione Curso</option>
	
	<%
		while(resultSet.next())
		{
			String fname = resultSet.getString("nomCurso"); 
	%>
	<option value="<%=fname %>"><%=fname %></option>
	<%
		}
	%>
	</select>
	<br>
	<div style="text-align: center">
			<input type="submit" value="Aceptar" class="btn btn-warning">
	</div>
		<%
		} catch (Exception e) {
		e.printStackTrace();
		}

%>
</form>
</body>
</html>