<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/bootstrap.min.css">

<title>Insert title here</title>
</head>
<body>
<body class="bg-dark">
	<div class="container">
		<div class="row justify-content-center"
			style="height: 90px;">
			<div class="col-0 align-self-end">
				<img src="img/usuarioDejarDeSeguir.png" width="80" height="80">
			</div>
			<div class="col-0 align-self-center">
				<h1 class="display-4 text-center pt-3" style="color: white">Dejar de Seguir
					un Usuario</h1>
			</div>
		</div>
	</div>
	<hr class="bg-warning">
	<form action="DejarSeguirValido.jsp" method="get">
		<div style="text-align: center; height: 50px;">
			<h3 class="text-light">Seleccionar:</h3>
		</div>
		<%
	String nickSesion = session.getAttribute("MiNick").toString();
	Class.forName("com.mysql.jdbc.Driver");
	java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
	Statement statement = null;
	ResultSet resultSet = null;
	String correo;
	String nick;
	try{
		statement=connection.createStatement();
		String sql;
		sql ="select * from usuario_sigue where nickname='" + nickSesion + "';";
		resultSet = statement.executeQuery(sql);
		%>
		<select name="selectUsu" class="form-control"
			style="width: 35%; margin: auto; text_align: center" size="8" required>
			<%
	while(resultSet.next()) {
		nick = resultSet.getString("nickname_seguido");
	%>
			<option value="<%=nick%>"><%=nick%></option>
			<%
	}
	%>
		</select> <br>
		<div style="text-align: center">
			<input type="submit" value="Dejar de seguir" class="btn btn-warning">
		</div>
		<%
		} catch (Exception e) {
		e.printStackTrace();
		}

%>
	</form>
</body>
</html>