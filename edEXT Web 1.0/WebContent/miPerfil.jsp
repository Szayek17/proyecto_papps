<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>edEXT:: Registrarse</title>

	<link rel="stylesheet" type="text/css" href="1-simple-calendar/tcal.css" />
	<script type="text/javascript" src="1-simple-calendar/tcal.js"></script>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/estilos.css">

</head>

<body class="bg-dark">
<h1 class="display-4 text-center pt-3">Mi Perfil.</h1>
<hr class="bg-warning">
<%
Statement statement = null;
ResultSet resultSet = null;
ResultSet resultSet2 = null;
Statement statement2 = null;
String nick = session.getAttribute("MiNick").toString();
session.setAttribute("MiNick", nick);
try{
java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
statement=connection.createStatement();
statement2=connection.createStatement();
String sql ="select * from usuario where nickname='" + nick +"';";
String sql2 = "select * from imagenesuser where nombreusuario='" + nick + "';";
resultSet = statement.executeQuery(sql);
resultSet2 = statement2.executeQuery(sql2);
while(resultSet.next() && resultSet2.next()){
%>	
					<div id="content">
  					<img src=<%=resultSet2.getString("rutaimagen") %> alt="" width="240" height="240" style="float:left; padding-top:80px; padding-left: 80px"/>
				</div>		
<div style="padding: 30px 100px 50px 300px;">

			
								

				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Nickname:</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="nickname" id="nickname" value=<%=resultSet.getString("nickname") %> readonly>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Nombre:</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="nombre" id="nombre" value=<%=resultSet.getString("nombre") %> readonly>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Apellido:</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="apellido" id="apellido" value=<%=resultSet.getString("apellido") %> readonly>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Correo electronico:</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="correo" id="correo" value=<%=resultSet.getString("correo") %> readonly>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Fecha de Nacimiento:</label>
					<div class="col-sm-9">
						<input type="text" name="date" class = "form-control" value=<%=resultSet.getString("fecha") %> readonly>
					</div>
				</div>

	

</div>
				
					<div style="text-align: center">
						<a class="btn btn-warning" style="color:black" id="eti" onclick="validar()">Modificar Datos.</a>
						<a class="btn btn-warning" style="color:black" id="eto" onclick="vermas()">Ver Mas.</a>
					</div>

<%
}
connection.close();
} catch (Exception e) {
e.printStackTrace();
}
%>

    <script>
        function validar() {
                document.getElementById("eti").setAttribute("href", "http://localhost:8080/Proyecto_full/modUsuario.jsp");
        }
        function vermas() {
    		document.getElementById("eto").setAttribute("href", "http://localhost:8080/Proyecto_full/datosAdEstudiante.jsp");
    }
    </script>
</body>
</html>