<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

	<%@ page import = "java.util.*" %>
	<%@ page import="org.json.simple.*" %>
	
	<%	
		Class.forName("com.mysql.cj.jdbc.Driver");
		java.sql.Connection miConexion = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
		java.sql.PreparedStatement c_preparada = miConexion.prepareStatement("SELECT * FROM institutobrindacurso;"); 
		java.sql.ResultSet resultadoObtenido = c_preparada.executeQuery();

		JSONObject json = new JSONObject();
		JSONObject cursoInst = new JSONObject();
		
		JSONArray  cursos = new JSONArray();
		JSONArray  inst = new JSONArray();
		//paso datos de cursos a un json
		while(resultadoObtenido.next()){ 
			String cur = resultadoObtenido.getString(2);
			String insti = resultadoObtenido.getString(1);
			inst.add(insti);
			cursos.add(cur);
		}
		
		cursoInst.put("instituto", inst);
		cursoInst.put("cursos", cursos);
		
		json.put("todo", cursoInst);
		
		response.setContentType("application/json");
		response.getWriter().write(json.toString());
		%>
		
