<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>edEXT:: Registrarse</title>

	<link rel="stylesheet" type="text/css" href="1-simple-calendar/tcal.css" />
	<script type="text/javascript" src="1-simple-calendar/tcal.js"></script>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/estilos.css">

</head>

<body class="bg-dark">
<iframe name="hiddenFrame" width="0" height="0" border="0" style="display: none;"></iframe>	<div class="container">
<h1 class="display-4 text-center pt-3">Edicion de Datos.</h1>
<hr class="bg-warning">	
			<form action="Altera_Datos.jsp" method="post">
			<%	String nick = session.getAttribute("MiNick").toString(); 
				session.setAttribute("NickMod", nick);
			%>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Nombre:</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="nombre" id="nombre">
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Apellido:</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="apellido" id="apellido">
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Instituto:</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="instituto" id="instituto">
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Fecha de Nacimiento:</label>
					<div class="col-sm-9">
						<input type="text" name="date" class = "form-control">
					</div>
				</div>
			</div>
				
					<div style="text-align: center">
						<button class="btn btn-warning" type="submit" style="color:black" id="eti" target="_parent">Aceptar</a>
					</div>
			</form>
</body>
</html>