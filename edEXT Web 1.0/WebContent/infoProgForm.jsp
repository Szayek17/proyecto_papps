<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>edEXT:: Registrarse</title>

	<link rel="stylesheet" type="text/css" href="1-simple-calendar/tcal.css" />
	<script type="text/javascript" src="1-simple-calendar/tcal.js"></script>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/estilos.css">

</head>

<body class="bg-dark">
<h1 class="display-4 text-center pt-3">Datos del Programa</h1>
<hr class="bg-warning">
<form action="InfoCurso.jsp" method="post">
<%
Statement statement = null;
Statement statement2 = null;
ResultSet resultSet = null;
ResultSet resultSet2 = null;
String nombre = request.getParameter("test");
try{
java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
statement=connection.createStatement();
statement2=connection.createStatement();
String sql ="select * from programaformacion where nombre='" + nombre + "';";
String sql2="select * from cursosintprogramas where nombreProg='" + nombre + "';";
resultSet = statement.executeQuery(sql);
resultSet2 = statement2.executeQuery(sql2);
while(resultSet.next()){
%>	
			
<div style="padding: 30px 100px 50px 300px;">

			
								

				
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Nombre:</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="nombre" id="nombre" value="<%=resultSet.getString("nombre") %>" readonly>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Descripcion:</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="descripcion" id="descripcion" value="<%=resultSet.getString("descripcion") %>" readonly>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Fecha de Inicio:</label>
					<div class="col-sm-9">
						<input type="text" name="date" class = "form-control" value=<%=resultSet.getString("fIni") %> readonly>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Fecha Fin:</label>
					<div class="col-sm-9">
						<input type="text" name="date2" class = "form-control" value=<%=resultSet.getString("fFin") %> readonly>
					</div>
				</div>
				<div class="row form-goup">
					<label class="col-form-label col-md-2" for="formGroupExampleInput">Cursos:</label>
					<div class="col-sm-9">
						<select name = "selectCurso" class="form-control" >
								<%
									while(resultSet2.next())
									{
										String fname = resultSet2.getString("nombreCurso"); 
								%>
								<option value="<%=fname %>"><%=fname %></option>
								<%
									}
								%>
						</select>
						<br>
						<div style="text-align: center">
							<input type="submit" value="Aceptar" class="btn btn-warning">
						</div>
					</div>
				</div>

	</div>

<%
}
connection.close();
} catch (Exception e) {
e.printStackTrace();
}
%>
</form>
</body>
</html>