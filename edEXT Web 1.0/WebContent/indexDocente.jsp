<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">

	<style>
		.dropdown-menu {
			background: #ffd04c;
		}

		a {
			color: darkblue;
		}

		.dropdown-item:hover,
		.dropdown-item:focus {
			color: #16181b;
			text-decoration: none;
			background-color: dimgray;
		}

		.img-circle {
			width: 50px;
			height: 50px;
			max-width: 100px;
			max-height: 100px;
			-webkit-border-radius: 50%;
			-moz-border-radius: 50%;
			border-radius: 50%;
			border: 3px solid rgba(234, 182, 8, 0.91);
		}
	</style>
</head>
<% 

String nickname = session.getAttribute("NickIniciado").toString();
session.setAttribute("MiNick", nickname);
ResultSet resultSet2 = null;
Statement statement2 = null;
try{
java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/instituto", "progapp", "pass");
statement2=connection.createStatement();
String sql2 = "select * from imagenesuser where nombreusuario='" + nickname + "';";
resultSet2 = statement2.executeQuery(sql2);
while(resultSet2.next()){
%>
<body class="bg-dark">
	<div class="header">
		<div>
			<nav class="navbar navbar-dark bg-dark">
				<div class="container-fluid">
					<div class="navbar-header">
						<a href="indexDocente.jsp"><img src="img/logo.png" width="30%" height="30%" align="left"/></a>
					</div>
					<nav class="navbar navbar bg-dark" style="right:15em">
						<form class="form-inline">
							<input class="form-control mr-sm-2 text-dark" type="search" placeholder="Search"
								aria-label="Search">
							<button class="btn btn-outline-warning my-2 my-sm-0" type="submit">Search</button>
						</form>
					</nav>
					<div class="col-1" align="right">
						<div class="row float-right justify-content-center">
							<img src = <%=resultSet2.getString("rutaimagen") %> alt="" width="48" height="48" style="float:left;"/>
							<div class="col-12" align="center">
								<p class="text-warning"><%=session.getAttribute("NickIniciado").toString() %></p>
							</div>
						</div>
					</div>
				</div>
			</nav>
		</div>
		<div>
			<nav class="navbar navbar-expand-sm bg-warning navbar-light">
				<ul class="navbar-nav">
					<li class="nav-item">
						<div class="btn-group">
							<button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">
								Usuarios
							</button>
							<div class="dropdown-menu">
								<a class="nav-link dropdown-item" href="miPerfilDocente.jsp" target="principal">Mi Perfil</a>
								<a id="btnCagar" class="nav-link dropdown-item" href="ConsultaUsuario.jsp"
									target="principal">Consultar
									Usuarios</a>
								<a id="btnCagar" class="nav-link dropdown-item" href="SeguirUsuario.jsp"
									target="principal">Seguir un Usuario</a> 
									<a id="btnCagar" class="nav-link dropdown-item" href="DejarSeguirUsuario.jsp"
									target="principal">Dejar de Seguir un Usuario</a>
							</div>
						</div>
					</li>
					<li class="nav-item">
						<div class="btn-group">
							<button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">
								Cursos
							</button>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="altaCurso.jsp" target="principal">Alta Curso</a>
								<a class="dropdown-item" href="AltaEdicioCurso.html" target="principal">Alta Edicion</a>
								<a class="dropdown-item" href="listarProg_Formacion.jsp" target="principal">Agregar Curso a Programa de Formacion</a>
															<div >	
								<a class="dropdown-item" href="ConsultaCurso.jsp" target="principal">	
									Consulta de Curso</a>	
									<a
									class="dropdown-item" href="consultaEdicionCurso.jsp"
									target="principal"> Consulta de Edicion de Curso</a>
																		<a
									class="dropdown-item" href="seleccionEdCurso.jsp"
									target="principal"> Agregar Est. a Ed. Curso</a>
							</div>
						</div>
					</li>
					<li class="nav-item">
						<div class="btn-group">
							<button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">
								Programas de Formacion
							</button>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="consultaProgForm.jsp" target="principal">	
									Consulta de Progama de Formacion</a>	
								<a class="dropdown-item" href="crearFormacion.html" target="principal">Alta Programa de Formacion</a>
									
							</div>
						</div>
					</li>
				</ul>
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link" href="indexVisitante.html" target="_parent" align-right>Salir</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

	<div class="row">
		<div class="embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="contenidoPrincipal.html" name="principal"></iframe>
		</div>
	</div>
	<%
}
connection.close();
} catch (Exception e) {
e.printStackTrace();
}
%>
		<!--Codigo Bootstrap-->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
			integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
			crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
			integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
			crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
			integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
			crossorigin="anonymous"></script>
		<script src="js/datos.js"></script>
		<script src="js/consulta.js"></script>
		<script>listarInstitutos();
				listarCategorias();
				localStorage.setItem("usuario", "heisenberg")
		</script>
</body>

</html>
