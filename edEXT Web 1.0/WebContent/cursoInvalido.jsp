<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>edEXT</title>

	<link rel="stylesheet" type="text/css" href="1-simple-calendar/tcal.css" />
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	
	<script type="text/javascript" src="1-simple-calendar/tcal.js"></script>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/estilos.css">

</head>
<body class=bg-dark>
	<div>
		<h1 class="display-4 text-center pt-3">Curso ya registrado.</h1>
		<hr class="bg-warning">
		<br><br>
		<div class="w3-container w3-center w3-animate-zoom">
			<img src="img/exclamation.png" class="center">
			<br><br>
			<table align="center">
			<tr>
    		<td colspan="2" align="center"><a class="btn btn-warning" style="color:black" id="eti" onclick="validar()">Aceptar</a>
</td>
    		</tr>
			</table>
		</div>
	</div>
	
<script type="text/javascript">
 	function validar() {
        document.getElementById("eti").setAttribute("href", "http://localhost:8080/Proyecto_full/altaCurso.jsp");
    };
</script>
</body>
</html>